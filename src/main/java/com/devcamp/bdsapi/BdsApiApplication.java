package com.devcamp.bdsapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BdsApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BdsApiApplication.class, args);
	}

}
