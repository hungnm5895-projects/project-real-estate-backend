package com.devcamp.bdsapi.Entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Value;

@Entity
@Table(name = "project")
public class CProject {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "_name", nullable = false)
    private String name;

    @Column(name = "_province_id")
    private int provinceId;

    @Column(name = "_district_id")
    private int districtId;

    @Column(name = "_ward_id")
    private int wardId;

    @Column(name = "_street_id")
    private int streetId;
    private String address;
    private String slogan;
    private String description;
    private BigDecimal acreage;

    @Column(name = "construct_area")
    private BigDecimal constructArea;

    @Column(name = "num_block")
    private int numBlock;

    @Column(name = "num_floors")
    private String numFloors;

    @Column(name = "num_apartment")
    private int numApartment;

    @Column(name = "apartmentt_area")
    private String apartmenttArea;
    private int investor;

    @Column(name = "construction_contractor")
    @Value("0")
    private int constructionContractor;

    @Column(name = "design_unit")
    private int designUnit;
    private String utilities;

    @Column(name = "region_link")
    private String regionLink;
    private String photo;

    @Column(name = "_lat")
    private double lat;

    @Column(name = "_lng")
    private double lng;

    public CProject() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(int provinceId) {
        this.provinceId = provinceId;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public int getWardId() {
        return wardId;
    }

    public void setWardId(int wardId) {
        this.wardId = wardId;
    }

    public int getStreetId() {
        return streetId;
    }

    public void setStreetId(int streetId) {
        this.streetId = streetId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getAcreage() {
        return acreage;
    }

    public void setAcreage(BigDecimal acreage) {
        this.acreage = acreage;
    }

    public BigDecimal getConstructArea() {
        return constructArea;
    }

    public void setConstructArea(BigDecimal constructArea) {
        this.constructArea = constructArea;
    }

    public int getNumBlock() {
        return numBlock;
    }

    public void setNumBlock(int numBlock) {
        this.numBlock = numBlock;
    }

    public String getNumFloors() {
        return numFloors;
    }

    public void setNumFloors(String numFloors) {
        this.numFloors = numFloors;
    }

    public int getNumApartment() {
        return numApartment;
    }

    public void setNumApartment(int numApartment) {
        this.numApartment = numApartment;
    }

    public String getApartmenttArea() {
        return apartmenttArea;
    }

    public void setApartmenttArea(String apartmenttArea) {
        this.apartmenttArea = apartmenttArea;
    }

    public int getInvestor() {
        return investor;
    }

    public void setInvestor(int investor) {
        this.investor = investor;
    }

    public int getConstructionContractor() {
        return constructionContractor;
    }

    public void setConstructionContractor(int constructionContractor) {
        this.constructionContractor = constructionContractor;
    }

    public int getDesignUnit() {
        return designUnit;
    }

    public void setDesignUnit(int designUnit) {
        this.designUnit = designUnit;
    }

    public String getUtilities() {
        return utilities;
    }

    public void setUtilities(String utilities) {
        this.utilities = utilities;
    }

    public String getRegionLink() {
        return regionLink;
    }

    public void setRegionLink(String regionLink) {
        this.regionLink = regionLink;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    

    

}
