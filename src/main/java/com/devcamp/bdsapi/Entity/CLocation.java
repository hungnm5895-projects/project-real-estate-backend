package com.devcamp.bdsapi.Entity;

import javax.persistence.*;

@Entity
@Table(name = "locations")
public class CLocation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "Latitude")
    private float latitude;

    @Column(name = "Longitude")
    private float longitude;

    public CLocation() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    
}
