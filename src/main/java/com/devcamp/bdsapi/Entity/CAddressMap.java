package com.devcamp.bdsapi.Entity;

import javax.persistence.*;

@Entity
@Table(name = "address_map")
public class CAddressMap {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String address;

    private double _lat;
    private double _lng;

    public CAddressMap() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double get_lat() {
        return _lat;
    }

    public void set_lat(double _lat) {
        this._lat = _lat;
    }

    public double get_lng() {
        return _lng;
    }

    public void set_lng(double _lng) {
        this._lng = _lng;
    }

}
