package com.devcamp.bdsapi.Entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "master_layout")
public class CMasterLayout {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Cần nhập mặt bằng tầng")
    private String name;

    private String description;

    @Column(name = "project_id")
    private int projectId;

    @Positive(message = "Diện tích phải lớn hơn 0")
    private BigDecimal acreage;

    @Column(name = "apartment_list")
    private String apartmentList;

    private String photo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true, updatable = false)
    @CreatedDate
    @DateTimeFormat(pattern = "yyyy-MM-yy")
    private Date date_create;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    @LastModifiedDate
    @DateTimeFormat(pattern = "yyyy-MM-yy")
    private Date date_update;

    public CMasterLayout() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public BigDecimal getAcreage() {
        return acreage;
    }

    public void setAcreage(BigDecimal acreage) {
        this.acreage = acreage;
    }

    public String getApartmentList() {
        return apartmentList;
    }

    public void setApartmentList(String apartmentList) {
        this.apartmentList = apartmentList;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Date getDate_create() {
        return date_create;
    }

    public void setDate_create(Date date_create) {
        this.date_create = date_create;
    }

    public Date getDate_update() {
        return date_update;
    }

    public void setDate_update(Date date_update) {
        this.date_update = date_update;
    }

}
