package com.devcamp.bdsapi.Entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "realestate")
public class CRealEstate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Thiếu tiêu đề tin")
    private String title;

    @NotNull(message = "Chưa chọn loại bất động sản")
    @Range(min = 0, max = 5, message = "Loại bất động sản không hợp lệ")
    private int type;

    @PositiveOrZero(message = "Yêu cầu không hợp lệ")
    private int request;

    @Positive(message = "Tỉnh/Thành phố không hợp lệ")
    private int province_id;

    @Positive(message = "Quận/Huyện không hợp lệ")
    private int district_id;

    private int wards_id;
    private int street_id;

    private int project_id;

    @NotNull(message = "Thiếu địa chỉ chi tiết")
    private String address;
    @PositiveOrZero(message = "khách hàng không hợp lệ")
    private int customer_id;

    @NotNull(message = "Thiếu thông tin giá tiền")
    @PositiveOrZero(message = "Giá tiền không hợp lệ")
    private long price;
    private long price_min;
    private int price_time;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true, updatable = false)
    @DateTimeFormat(pattern = "yyyy-MM-yy")
    private Date date_create;

    @NotNull(message = "Thiếu diện tích bất động sản")
    @Positive(message = "Diện tích không hợp lệ")
    private BigDecimal acreage;

    @PositiveOrZero(message = "Hướng không hợp lệ")
    private int direction;

    @NotNull(message = "Thiếu thông tin số tầng")
    @Positive(message = "Tổng số tầng không hợp lệ")
    private int total_floors;

    @PositiveOrZero(message = "Tầng số không hợp lệ")
    private int number_floors;

    @PositiveOrZero(message = "Số nhà vệ sinh không hợp lệ")
    private int bath;
    private String apart_code;

    @Positive(message = "Diện tích tường không hợp lệ")
    private BigDecimal wall_area;

    @PositiveOrZero(message = "Số phòng ngủ không hợp lệ")
    private int bedroom;

    @Range(min = 0, max = 3, message = "Thông tin ban công không hợp lệ")
    private int balcony;
    private String landscape_view;

    @Range(min = 0, max = 2, message = "Vị trí căn hộ không hợp lệ")
    private int apart_loca;
    private int apart_type;

    @Range(min = 0, max = 3, message = "Thông tin nội thất không hợp lệ")
    private int furniture_type;
    private int price_rent;
    private double return_rate;
    private int legal_doc;
    private String description;
    @Positive(message = "Chiều rộng không hợp lệ")
    private int width_y;
    @Positive(message = "Chiều dài không hợp lệ")
    private int long_x;
    private int street_house;
    private int FSBO;
    private int view_num;
    private int create_by;
    private int update_by;
    private String shape;
    private int distance2facade;
    private int adjacent_facade_num;
    private String adjacent_road;
    private int alley_min_width;
    private int adjacent_alley_min_width;
    private int factor;
    private String structure;
    private int DTSXD;
    private int CLCL;
    private int CTXD_price;
    private int CTXD_value;
    private String photo;
    private double _lat;
    private double _lng;

    @Range(min = 0, max = 3, message = "Trạng thái bài đăng không hợp lệ")
    private int postStatus;
    private String reason;

    public CRealEstate() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getRequest() {
        return request;
    }

    public void setRequest(int request) {
        this.request = request;
    }

    public int getProvince_id() {
        return province_id;
    }

    public void setProvince_id(int province_id) {
        this.province_id = province_id;
    }

    public int getDistrict_id() {
        return district_id;
    }

    public void setDistrict_id(int district_id) {
        this.district_id = district_id;
    }

    public int getWards_id() {
        return wards_id;
    }

    public void setWards_id(int wards_id) {
        this.wards_id = wards_id;
    }

    public int getStreet_id() {
        return street_id;
    }

    public void setStreet_id(int street_id) {
        this.street_id = street_id;
    }

    public int getProject_id() {
        return project_id;
    }

    public void setProject_id(int project_id) {
        this.project_id = project_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public long getPrice_min() {
        return price_min;
    }

    public void setPrice_min(long price_min) {
        this.price_min = price_min;
    }

    public int getPrice_time() {
        return price_time;
    }

    public void setPrice_time(int price_time) {
        this.price_time = price_time;
    }

    public Date getDate_create() {
        return date_create;
    }

    public void setDate_create(Date date_create) {
        this.date_create = date_create;
    }

    public BigDecimal getAcreage() {
        return acreage;
    }

    public void setAcreage(BigDecimal acreage) {
        this.acreage = acreage;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public int getTotal_floors() {
        return total_floors;
    }

    public void setTotal_floors(int total_floors) {
        this.total_floors = total_floors;
    }

    public int getNumber_floors() {
        return number_floors;
    }

    public void setNumber_floors(int number_floors) {
        this.number_floors = number_floors;
    }

    public int getBath() {
        return bath;
    }

    public void setBath(int bath) {
        this.bath = bath;
    }

    public String getApart_code() {
        return apart_code;
    }

    public void setApart_code(String apart_code) {
        this.apart_code = apart_code;
    }

    public BigDecimal getWall_area() {
        return wall_area;
    }

    public void setWall_area(BigDecimal wall_area) {
        this.wall_area = wall_area;
    }

    public int getBedroom() {
        return bedroom;
    }

    public void setBedroom(int bedroom) {
        this.bedroom = bedroom;
    }

    public int getBalcony() {
        return balcony;
    }

    public void setBalcony(int balcony) {
        this.balcony = balcony;
    }

    public String getLandscape_view() {
        return landscape_view;
    }

    public void setLandscape_view(String landscape_view) {
        this.landscape_view = landscape_view;
    }

    public int getApart_loca() {
        return apart_loca;
    }

    public void setApart_loca(int apart_loca) {
        this.apart_loca = apart_loca;
    }

    public int getApart_type() {
        return apart_type;
    }

    public void setApart_type(int apart_type) {
        this.apart_type = apart_type;
    }

    public int getFurniture_type() {
        return furniture_type;
    }

    public void setFurniture_type(int furniture_type) {
        this.furniture_type = furniture_type;
    }

    public int getPrice_rent() {
        return price_rent;
    }

    public void setPrice_rent(int price_rent) {
        this.price_rent = price_rent;
    }

    public double getReturn_rate() {
        return return_rate;
    }

    public void setReturn_rate(double return_rate) {
        this.return_rate = return_rate;
    }

    public int getLegal_doc() {
        return legal_doc;
    }

    public void setLegal_doc(int legal_doc) {
        this.legal_doc = legal_doc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getWidth_y() {
        return width_y;
    }

    public void setWidth_y(int width_y) {
        this.width_y = width_y;
    }

    public int getLong_x() {
        return long_x;
    }

    public void setLong_x(int long_x) {
        this.long_x = long_x;
    }

    public int getStreet_house() {
        return street_house;
    }

    public void setStreet_house(int street_house) {
        this.street_house = street_house;
    }

    public int getFSBO() {
        return FSBO;
    }

    public void setFSBO(int fSBO) {
        FSBO = fSBO;
    }

    public int getView_num() {
        return view_num;
    }

    public void setView_num(int view_num) {
        this.view_num = view_num;
    }

    public int getCreate_by() {
        return create_by;
    }

    public void setCreate_by(int create_by) {
        this.create_by = create_by;
    }

    public int getUpdate_by() {
        return update_by;
    }

    public void setUpdate_by(int update_by) {
        this.update_by = update_by;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public int getDistance2facade() {
        return distance2facade;
    }

    public void setDistance2facade(int distance2facade) {
        this.distance2facade = distance2facade;
    }

    public int getAdjacent_facade_num() {
        return adjacent_facade_num;
    }

    public void setAdjacent_facade_num(int adjacent_facade_num) {
        this.adjacent_facade_num = adjacent_facade_num;
    }

    public String getAdjacent_road() {
        return adjacent_road;
    }

    public void setAdjacent_road(String adjacent_road) {
        this.adjacent_road = adjacent_road;
    }

    public int getAlley_min_width() {
        return alley_min_width;
    }

    public void setAlley_min_width(int alley_min_width) {
        this.alley_min_width = alley_min_width;
    }

    public int getAdjacent_alley_min_width() {
        return adjacent_alley_min_width;
    }

    public void setAdjacent_alley_min_width(int adjacent_alley_min_width) {
        this.adjacent_alley_min_width = adjacent_alley_min_width;
    }

    public int getFactor() {
        return factor;
    }

    public void setFactor(int factor) {
        this.factor = factor;
    }

    public String getStructure() {
        return structure;
    }

    public void setStructure(String structure) {
        this.structure = structure;
    }

    public int getDTSXD() {
        return DTSXD;
    }

    public void setDTSXD(int dTSXD) {
        DTSXD = dTSXD;
    }

    public int getCLCL() {
        return CLCL;
    }

    public void setCLCL(int cLCL) {
        CLCL = cLCL;
    }

    public int getCTXD_price() {
        return CTXD_price;
    }

    public void setCTXD_price(int cTXD_price) {
        CTXD_price = cTXD_price;
    }

    public int getCTXD_value() {
        return CTXD_value;
    }

    public void setCTXD_value(int cTXD_value) {
        CTXD_value = cTXD_value;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public double get_lat() {
        return _lat;
    }

    public void set_lat(double _lat) {
        this._lat = _lat;
    }

    public double get_lng() {
        return _lng;
    }

    public void set_lng(double _lng) {
        this._lng = _lng;
    }

    public int getPostStatus() {
        return postStatus;
    }

    public void setPostStatus(int postStatus) {
        this.postStatus = postStatus;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    
}
