package com.devcamp.bdsapi.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.bdsapi.Entity.CLocation;

public interface LocationRepository extends JpaRepository<CLocation, Integer>{
    
}
