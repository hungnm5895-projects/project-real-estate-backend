package com.devcamp.bdsapi.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.bdsapi.Entity.CAddressMap;

public interface AddressMapRepository extends JpaRepository<CAddressMap, Integer> {

}
