package com.devcamp.bdsapi.Respository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.bdsapi.Entity.CRegionLink;

public interface RegionLinkRepository extends JpaRepository<CRegionLink, Integer>{
    
}
