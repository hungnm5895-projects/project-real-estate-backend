package com.devcamp.bdsapi.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.bdsapi.Entity.CEmployees;

public interface EmployeesRepository extends JpaRepository<CEmployees, Integer>{
    
}
