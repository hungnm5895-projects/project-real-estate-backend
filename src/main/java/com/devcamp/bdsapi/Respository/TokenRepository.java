package com.devcamp.bdsapi.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.bdsapi.Entity.*;

public interface TokenRepository extends JpaRepository<Token, Long> {

    Token findByToken(String token);
}
