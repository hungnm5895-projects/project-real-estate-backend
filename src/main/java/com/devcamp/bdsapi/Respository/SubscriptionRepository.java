package com.devcamp.bdsapi.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.bdsapi.Entity.CSubscription;

public interface SubscriptionRepository extends JpaRepository<CSubscription, Integer>{
    
}
