package com.devcamp.bdsapi.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.bdsapi.Entity.CDistrict;

public interface DistrictRepository extends JpaRepository<CDistrict, Integer>{
    
}
