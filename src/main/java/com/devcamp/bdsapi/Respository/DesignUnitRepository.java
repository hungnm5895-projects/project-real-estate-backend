package com.devcamp.bdsapi.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.bdsapi.Entity.CDesignUnit;

public interface DesignUnitRepository extends JpaRepository<CDesignUnit, Integer>{
    
}
