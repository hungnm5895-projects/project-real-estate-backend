package com.devcamp.bdsapi.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.bdsapi.Entity.Role;


public interface RoleRespository extends JpaRepository<Role, Long>{
    Role findByRoleKey(String roleKey);

}
