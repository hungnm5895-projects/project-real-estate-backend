package com.devcamp.bdsapi.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.bdsapi.Entity.CContructionContractor;

public interface ContructionContractorRepositopy extends JpaRepository<CContructionContractor, Integer>{
    
}
