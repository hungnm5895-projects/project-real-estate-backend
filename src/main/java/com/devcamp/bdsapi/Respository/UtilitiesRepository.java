package com.devcamp.bdsapi.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.bdsapi.Entity.CUtilities;

public interface UtilitiesRepository extends JpaRepository<CUtilities, Integer>{
    
}
