package com.devcamp.bdsapi.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.bdsapi.Entity.CCustomer;

public interface CustomerRepository extends JpaRepository<CCustomer, Integer>{
    
}
