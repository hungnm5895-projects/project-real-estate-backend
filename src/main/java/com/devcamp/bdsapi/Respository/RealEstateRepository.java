package com.devcamp.bdsapi.Respository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.bdsapi.Entity.CRealEstate;

public interface RealEstateRepository extends JpaRepository<CRealEstate, Integer> {
        List<CRealEstate> findByPostStatus(int postStatus, Pageable pageable);

        List<CRealEstate> findAllByOrderByPostStatusAsc();

        @Query(value = "SELECT realestate.* FROM realestate WHERE realestate.post_status = 1 ORDER BY realestate.date_create DESC LIMIT 4", nativeQuery = true)
        List<CRealEstate> findHotRealEstate();

        @Query(value = "SELECT realestate.* FROM realestate WHERE (:type= -1 or realestate.type = :type) " +
                        "AND  (:request= -1 or realestate.request = :request)" +
                        "AND (:province_id= 0 or province_id = :province_id) " +
                        "AND (:district_id= 0 or district_id = :district_id) " +
                        "AND (:furniture_type= -1 or furniture_type = :furniture_type)" +
                        "AND post_status = 1", nativeQuery = true)
        List<CRealEstate> fliterRealEstate(
                        @Param("type") int type,
                        @Param("request") int request,
                        @Param("province_id") int province_id,
                        @Param("district_id") int district_id,
                        @Param("furniture_type") int furniture_type,
                        Pageable pageable);

        @Query(value = "SELECT * FROM realestate WHERE create_by = :id AND post_status = :post_status", nativeQuery = true)
        List<CRealEstate> findByIdAndByPoststatus(
                        @Param("id") int id,
                        @Param("post_status") int post_status,
                        Pageable pageable);

        @Query(value = "SELECT * FROM realestate WHERE create_by = :id ORDER BY post_status ASC", nativeQuery = true)
        List<CRealEstate> findByCreateId(
                        @Param("id") int id);

        @Query(value = "SELECT * FROM realestate WHERE customer_id = :id ORDER BY post_status ASC", nativeQuery = true)
        List<CRealEstate> findByCustomerId(
                        @Param("id") int id);

}
