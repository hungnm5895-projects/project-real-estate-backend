package com.devcamp.bdsapi.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.bdsapi.Entity.CWard;

public interface WardRepository extends JpaRepository<CWard, Integer> {
    
}
