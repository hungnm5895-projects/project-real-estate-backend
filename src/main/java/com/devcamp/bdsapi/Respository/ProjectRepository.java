package com.devcamp.bdsapi.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.bdsapi.Entity.CProject;
import java.util.List;


public interface ProjectRepository extends JpaRepository<CProject, Integer>{
    List<CProject> findByDistrictId(int districtId);
}
