package com.devcamp.bdsapi.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.bdsapi.Entity.CStreet;

public interface StreetRepository extends JpaRepository<CStreet, Integer>{
    
}
