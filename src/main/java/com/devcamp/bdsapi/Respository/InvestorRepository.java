package com.devcamp.bdsapi.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.bdsapi.Entity.CInvestor;

public interface InvestorRepository extends JpaRepository<CInvestor, Integer>{
    
}
