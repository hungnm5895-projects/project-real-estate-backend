package com.devcamp.bdsapi.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.bdsapi.Entity.CSubscription;
import com.devcamp.bdsapi.Respository.SubscriptionRepository;

@Service
public class SubscriptionService {
    @Autowired
    SubscriptionRepository subscriptionRepository;

    // Truy vấn toàn bộ Subscription
    public List<CSubscription> getAllSubscriptionService() {
        List<CSubscription> lstSubscriptions = new ArrayList<>();
        subscriptionRepository.findAll().forEach(lstSubscriptions::add);
        return lstSubscriptions;
    }

    // Truy vấn theo Subscription ID
    // Output: Optional
    public Optional<CSubscription> getSubscriptionByIdService(int id) {
        Optional<CSubscription> subscriptionOptional = subscriptionRepository.findById(id);
        return subscriptionOptional;
    }

    // Thêm mới Subscription
    public CSubscription createSubscriptionService(CSubscription cSubscription) {
        CSubscription newSubscription = new CSubscription();
        newSubscription.setUser(cSubscription.getUser());
        newSubscription.setAuthenticationtoken(cSubscription.getAuthenticationtoken());
        newSubscription.setContentencoding(cSubscription.getContentencoding());
        newSubscription.setEndpoint(cSubscription.getEndpoint());

        newSubscription = subscriptionRepository.save(newSubscription);
        return newSubscription;
    }

    // Sửa Subscription
    public CSubscription updateSubscriptionService(int id, CSubscription cSubscription) {
        Optional<CSubscription> subOptional = subscriptionRepository.findById(id);
        if (subOptional.isPresent()) {
            CSubscription newSubscription = subOptional.get();
            newSubscription.setUser(cSubscription.getUser());
            newSubscription.setAuthenticationtoken(cSubscription.getAuthenticationtoken());
            newSubscription.setContentencoding(cSubscription.getContentencoding());
            newSubscription.setEndpoint(cSubscription.getEndpoint());

            newSubscription = subscriptionRepository.save(newSubscription);
            return newSubscription;
        } else {
            return null;
        }
    }

    // Xóa Subscription theo Id
    public void deleteSubscriptionService(int id) {
        subscriptionRepository.deleteById(id);
    }
}
