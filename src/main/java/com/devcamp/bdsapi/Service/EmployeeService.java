package com.devcamp.bdsapi.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.bdsapi.Entity.CEmployees;
import com.devcamp.bdsapi.Respository.EmployeesRepository;

@Service
public class EmployeeService {
    @Autowired
    EmployeesRepository employeesRepository;

    // Truy vấn toàn bộ Employees
    public List<CEmployees> getAllEmployeesService() {
        List<CEmployees> lstEmployees = new ArrayList<>();
        employeesRepository.findAll().forEach(lstEmployees::add);
        return lstEmployees;
    }

    // Truy vấn theo Employees ID
    // Output: Optional
    public Optional<CEmployees> getEmployeesByIdService(int id) {
        Optional<CEmployees> employeesOptional = employeesRepository.findById(id);
        return employeesOptional;
    }

    // Thêm mới Employees
    public CEmployees createEmployeesService(CEmployees cEmployees) {
        CEmployees newEmployees = new CEmployees();
        newEmployees.setFirstName(cEmployees.getFirstName());
        newEmployees.setLastName(cEmployees.getLastName());
        newEmployees.setTitle(cEmployees.getTitle());
        newEmployees.setTitleOfCourtesy(cEmployees.getTitleOfCourtesy());
        newEmployees.setBirthDate(cEmployees.getBirthDate());
        newEmployees.setHireDate(cEmployees.getHireDate());
        newEmployees.setAddress(cEmployees.getAddress());
        newEmployees.setCity(cEmployees.getCity());
        newEmployees.setRegion(cEmployees.getRegion());
        newEmployees.setPostalCode(cEmployees.getPostalCode());
        newEmployees.setCountry(cEmployees.getCountry());
        newEmployees.setHomePhone(cEmployees.getHomePhone());
        newEmployees.setExtension(cEmployees.getExtension());
        newEmployees.setPhoto(cEmployees.getPhoto());
        newEmployees.setNotes(cEmployees.getNotes());
        newEmployees.setReportsTo(cEmployees.getReportsTo());
        newEmployees.setUsername(cEmployees.getUsername());
        newEmployees.setPassword(cEmployees.getPassword());
        newEmployees.setEmail(cEmployees.getEmail());
        newEmployees.setActivated("Y");
        newEmployees.setProfile(cEmployees.getProfile());
        newEmployees.setUserLevel(0);

        newEmployees = employeesRepository.save(newEmployees);
        return newEmployees;
    }

    // Sửa Employees
    public CEmployees updateEmployeesService(int id, CEmployees cEmployees) {
        Optional<CEmployees> employeesOptional = employeesRepository.findById(id);
        if (employeesOptional.isPresent()) {
            CEmployees newEmployees = employeesOptional.get();
           newEmployees.setFirstName(cEmployees.getFirstName());
        newEmployees.setLastName(cEmployees.getLastName());
        newEmployees.setTitle(cEmployees.getTitle());
        newEmployees.setTitleOfCourtesy(cEmployees.getTitleOfCourtesy());
        newEmployees.setBirthDate(cEmployees.getBirthDate());
        newEmployees.setHireDate(cEmployees.getHireDate());
        newEmployees.setAddress(cEmployees.getAddress());
        newEmployees.setCity(cEmployees.getCity());
        newEmployees.setRegion(cEmployees.getRegion());
        newEmployees.setPostalCode(cEmployees.getPostalCode());
        newEmployees.setCountry(cEmployees.getCountry());
        newEmployees.setHomePhone(cEmployees.getHomePhone());
        newEmployees.setExtension(cEmployees.getExtension());
        newEmployees.setPhoto(cEmployees.getPhoto());
        newEmployees.setNotes(cEmployees.getNotes());
        newEmployees.setReportsTo(cEmployees.getReportsTo());
        newEmployees.setUsername(cEmployees.getUsername());
        newEmployees.setPassword(cEmployees.getPassword());
        newEmployees.setEmail(cEmployees.getEmail());
        newEmployees.setActivated(cEmployees.getActivated());
        newEmployees.setProfile(cEmployees.getProfile());
        newEmployees.setUserLevel(cEmployees.getUserLevel());

            newEmployees = employeesRepository.save(newEmployees);
            return newEmployees;
        } else {
            return null;
        }
    }

    // Xóa Employees theo Id
    public void deleteEmployeesService(int id) {
        employeesRepository.deleteById(id);
    }
}
