package com.devcamp.bdsapi.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.bdsapi.Entity.CCustomer;
import com.devcamp.bdsapi.Respository.CustomerRepository;

@Service
public class CustomerService {
    @Autowired
    CustomerRepository customerRepository;

    // Truy vấn toàn bộ Customer
    public List<CCustomer> getAllCustomerService() {
        List<CCustomer> lstCustomers = new ArrayList<>();
        customerRepository.findAll().forEach(lstCustomers::add);
        return lstCustomers;
    }

    // Truy vấn theo Customer ID
    // Output: Optional
    public Optional<CCustomer> getCustomerByIdService(int id) {
        Optional<CCustomer> customerOptional = customerRepository.findById(id);
        return customerOptional;
    }

    // Thêm mới Customer
    public CCustomer createCustomerService(CCustomer cCustomer) {
        CCustomer newCustomer = new CCustomer();
        newCustomer.setContactName(cCustomer.getContactName());
        newCustomer.setAddress(cCustomer.getAddress());
        newCustomer.setContactTitle(cCustomer.getContactTitle());
        newCustomer.setEmail(cCustomer.getEmail());
        newCustomer.setMobile(cCustomer.getMobile());
        newCustomer.setCreateBy(cCustomer.getCreateBy());
        newCustomer.setNote(cCustomer.getNote());
        newCustomer.setUpdateBy(cCustomer.getUpdateBy());
        newCustomer.setCreate_date(new Date());
        newCustomer = customerRepository.save(newCustomer);
        return newCustomer;
    }

    // Sửa Customer
    public CCustomer updateCustomerService(int id, CCustomer cCustomer) {
        Optional<CCustomer> customer = customerRepository.findById(id);
        if (customer.isPresent()) {
            CCustomer newCustomer = customer.get();
            newCustomer.setContactName(cCustomer.getContactName());
            newCustomer.setAddress(cCustomer.getAddress());
            newCustomer.setContactTitle(cCustomer.getContactTitle());
            newCustomer.setEmail(cCustomer.getEmail());
            newCustomer.setMobile(cCustomer.getMobile());
            newCustomer.setCreateBy(cCustomer.getCreateBy());
            newCustomer.setNote(cCustomer.getNote());
            newCustomer.setUpdateBy(cCustomer.getUpdateBy());
            newCustomer.setUpdate_date(new Date());

            newCustomer = customerRepository.save(newCustomer);
            return newCustomer;
        } else {
            return null;
        }
    }

    // Xóa Customer theo Id
    public void deleteCustomerService(int id) {
        customerRepository.deleteById(id);
    }
}
