package com.devcamp.bdsapi.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.bdsapi.Entity.CMasterLayout;
import com.devcamp.bdsapi.Respository.MasterLayoutRepository;

@Service
public class MasterLayoutService {
    @Autowired
    MasterLayoutRepository masterLayoutRepository;

    // Truy vấn toàn bộ
    public List<CMasterLayout> getAllMasterLayoutService() {
        List<CMasterLayout> lstMaster = new ArrayList<>();
        masterLayoutRepository.findAll().forEach(lstMaster::add);
        return lstMaster;
    }

    // Truy vấn theo ID
    // Output: Optional
    public Optional<CMasterLayout> getMasterLayoutByIdService(int id) {
        Optional<CMasterLayout> masterOptional = masterLayoutRepository.findById(id);
        return masterOptional;
    }

    // Thêm mới
    public CMasterLayout createMasterLayoutService(CMasterLayout cMasterLayout) {
        CMasterLayout newMaster = new CMasterLayout();
        newMaster.setName(cMasterLayout.getName());
        newMaster.setDescription(cMasterLayout.getDescription());
        newMaster.setAcreage(cMasterLayout.getAcreage());
        newMaster.setApartmentList(cMasterLayout.getApartmentList());
        newMaster.setProjectId(cMasterLayout.getProjectId());
        newMaster.setDate_create(new Date());

        newMaster = masterLayoutRepository.save(newMaster);
        return newMaster;
    }

    // Sửa Investor
    public CMasterLayout updateMasterLayoutService(int id, CMasterLayout cMasterLayout) {
        Optional<CMasterLayout> sOptional = masterLayoutRepository.findById(id);
        if (sOptional.isPresent()) {
            CMasterLayout newMaster = sOptional.get();
            newMaster.setName(cMasterLayout.getName());
            newMaster.setDescription(cMasterLayout.getDescription());
            newMaster.setAcreage(cMasterLayout.getAcreage());
            newMaster.setApartmentList(cMasterLayout.getApartmentList());
            newMaster.setProjectId(cMasterLayout.getProjectId());
            newMaster.setDate_update(new Date());
            newMaster = masterLayoutRepository.save(newMaster);
            return newMaster;
        } else {
            return null;
        }
    }

    // Xóa theo Id
    public void deleteMasterLayoutService(int id) {
        masterLayoutRepository.deleteById(id);
    }
}
