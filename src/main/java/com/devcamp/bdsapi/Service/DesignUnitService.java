package com.devcamp.bdsapi.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.bdsapi.Entity.CDesignUnit;
import com.devcamp.bdsapi.Respository.DesignUnitRepository;

@Service
public class DesignUnitService {
    @Autowired
    DesignUnitRepository designUnitRepository;

    // Truy vấn toàn bộ Design Unit
    public List<CDesignUnit> getAllDesginUnitService() {
        List<CDesignUnit> lstDeUnit = new ArrayList<>();
        designUnitRepository.findAll().forEach(lstDeUnit::add);
        return lstDeUnit;
    }

    // Truy vấn Design Unit theo ID
    // Output: Optional Design Unit
    public Optional<CDesignUnit> getDesignServiceByIdService(int id) {
        Optional<CDesignUnit> designUniteOptional = designUnitRepository.findById(id);
        return designUniteOptional;
    }

    // Thêm mới Design Unit
    public CDesignUnit createDesignUnitService(CDesignUnit cDesignUnit) {
        CDesignUnit newDesignUnit = new CDesignUnit();
        newDesignUnit.setName(cDesignUnit.getName());
        newDesignUnit.setDescription(cDesignUnit.getDescription());
        newDesignUnit.setProjects(cDesignUnit.getProjects());
        newDesignUnit.setAddress(cDesignUnit.getAddress());
        newDesignUnit.setPhone(cDesignUnit.getPhone());
        newDesignUnit.setPhone2(cDesignUnit.getPhone2());
        newDesignUnit.setFax(cDesignUnit.getFax());
        newDesignUnit.setEmail(cDesignUnit.getEmail());
        newDesignUnit.setWebsite(cDesignUnit.getWebsite());
        newDesignUnit.setNote(cDesignUnit.getNote());
        newDesignUnit = designUnitRepository.save(newDesignUnit);
        return newDesignUnit;
    }

    // Sửa Design Unit
    public CDesignUnit updateDesignUnitService(int id, CDesignUnit cDesignUnit) {
        Optional<CDesignUnit> saveDesignUnitOptional = designUnitRepository.findById(id);
        if (saveDesignUnitOptional.isPresent()) {
            CDesignUnit newDesignUnit = saveDesignUnitOptional.get();
            newDesignUnit.setName(cDesignUnit.getName());
            newDesignUnit.setDescription(cDesignUnit.getDescription());
            newDesignUnit.setProjects(cDesignUnit.getProjects());
            newDesignUnit.setAddress(cDesignUnit.getAddress());
            newDesignUnit.setPhone(cDesignUnit.getPhone());
            newDesignUnit.setPhone2(cDesignUnit.getPhone2());
            newDesignUnit.setFax(cDesignUnit.getFax());
            newDesignUnit.setEmail(cDesignUnit.getEmail());
            newDesignUnit.setWebsite(cDesignUnit.getWebsite());
            newDesignUnit.setNote(cDesignUnit.getNote());
            newDesignUnit = designUnitRepository.save(newDesignUnit);
            return newDesignUnit;
        } else {
            return null;
        }
    }

    //Xóa design Unit theo Id
    public void deleteDesignUnitService(int id) {
        designUnitRepository.deleteById(id);
    }
}
