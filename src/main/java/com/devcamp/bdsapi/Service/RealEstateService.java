package com.devcamp.bdsapi.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.bdsapi.Entity.CRealEstate;
import com.devcamp.bdsapi.Respository.RealEstateRepository;

@Service
public class RealEstateService {
    @Autowired
    RealEstateRepository realEstateRepository;

    // Truy vấn toàn bộ RealEstate + sắp xếp theo post status tăng dần
    public List<CRealEstate> getAllRealEstateService() {
        List<CRealEstate> lstRealEstate = new ArrayList<>();
        realEstateRepository.findAllByOrderByPostStatusAsc().forEach(lstRealEstate::add);
        return lstRealEstate;
    }

    // Truy vấn toàn bộ RealEstate phân trang
    public List<CRealEstate> getAllRealEstatePagingService(String page, String size) {
        Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
        List<CRealEstate> lstRealEstate = new ArrayList<>();
        realEstateRepository.findAll(pageWithFiveElements).forEach(lstRealEstate::add);
        return lstRealEstate;
    }

    // Truy vấn toàn bộ RealEstate phân trang tại trang chủ
    public List<CRealEstate> getAllRealEstatePagingIndexService(String page, String size) {
        Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
        List<CRealEstate> lstRealEstate = new ArrayList<>();
        realEstateRepository.findByPostStatus(1, pageWithFiveElements).forEach(lstRealEstate::add);
        return lstRealEstate;
    }

    // Filter RealEstate phân trang tại trang chủ
    public List<CRealEstate> getFilterRealEstatePagingService(String page, String size, int type, int request,
            int Province_id, int District_id, int furniture_type) {
        Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
        List<CRealEstate> lstRealEstate = realEstateRepository.fliterRealEstate(type, request, Province_id, District_id,
                furniture_type, pageWithFiveElements);
        return lstRealEstate;
    }

    // Truy vấn theo RealEstate ID
    // Output: Optional
    public Optional<CRealEstate> getRealEstateByIdService(int id) {
        Optional<CRealEstate> realEstateOptional = realEstateRepository.findById(id);
        return realEstateOptional;
    }

    // Truy vấn RealEstate hot
    public List<CRealEstate> getHotRealEstatePagingService() {
        List<CRealEstate> lstRealEstate = new ArrayList<>();
        lstRealEstate = realEstateRepository.findHotRealEstate();
        return lstRealEstate;
    }

    // Thêm mới RealEstate
    public CRealEstate createRealEstateService(CRealEstate cRealEstate) {
        CRealEstate newRealEstate = new CRealEstate();
        newRealEstate.setTitle(cRealEstate.getTitle());
        newRealEstate.setType(cRealEstate.getType());
        newRealEstate.setRequest(cRealEstate.getRequest());
        newRealEstate.setProject_id(cRealEstate.getProject_id());
        newRealEstate.setProvince_id(cRealEstate.getProvince_id());
        newRealEstate.setDistrict_id(cRealEstate.getDistrict_id());
        newRealEstate.setWards_id(cRealEstate.getWards_id());
        newRealEstate.setStreet_id(cRealEstate.getStreet_id());
        newRealEstate.setAddress(cRealEstate.getAddress());
        newRealEstate.setCustomer_id(cRealEstate.getCustomer_id());
        newRealEstate.setPrice(cRealEstate.getPrice());
        newRealEstate.setPrice_min(cRealEstate.getPrice_min());
        newRealEstate.setPrice_time(cRealEstate.getPrice_time());
        newRealEstate.setDate_create(new Date());
        newRealEstate.setAcreage(cRealEstate.getAcreage());
        newRealEstate.setDescription(cRealEstate.getDescription());
        newRealEstate.setTotal_floors(cRealEstate.getTotal_floors());
        newRealEstate.setNumber_floors(cRealEstate.getNumber_floors());
        newRealEstate.setBath(cRealEstate.getBath());
        newRealEstate.setApart_code(cRealEstate.getApart_code());
        newRealEstate.setWall_area(cRealEstate.getWall_area());
        newRealEstate.setBedroom(cRealEstate.getBedroom());
        newRealEstate.setBalcony(cRealEstate.getBalcony());
        newRealEstate.setLandscape_view(cRealEstate.getLandscape_view());
        newRealEstate.setApart_code(cRealEstate.getApart_code());
        newRealEstate.setApart_type(cRealEstate.getApart_type());
        newRealEstate.setFurniture_type(cRealEstate.getFurniture_type());
        newRealEstate.setPrice_rent(cRealEstate.getPrice_rent());
        newRealEstate.setReturn_rate(cRealEstate.getReturn_rate());
        newRealEstate.setLegal_doc(cRealEstate.getLegal_doc());
        newRealEstate.setDirection(cRealEstate.getDirection());
        newRealEstate.setWidth_y(cRealEstate.getWidth_y());
        newRealEstate.setLong_x(cRealEstate.getLong_x());
        newRealEstate.setStreet_house(cRealEstate.getStreet_house());
        newRealEstate.setFSBO(cRealEstate.getFSBO());
        newRealEstate.setView_num(0);
        newRealEstate.setCreate_by(cRealEstate.getCreate_by());
        newRealEstate.setUpdate_by(cRealEstate.getUpdate_by());
        newRealEstate.setShape(cRealEstate.getShape());
        newRealEstate.setDistance2facade(cRealEstate.getDistance2facade());
        newRealEstate.setAdjacent_alley_min_width(cRealEstate.getAdjacent_alley_min_width());
        newRealEstate.setAdjacent_road(cRealEstate.getAdjacent_road());
        newRealEstate.setAlley_min_width(cRealEstate.getAlley_min_width());
        newRealEstate.setFactor(cRealEstate.getFactor());
        newRealEstate.setStructure(cRealEstate.getStructure());
        newRealEstate.setDTSXD(cRealEstate.getDTSXD());
        newRealEstate.setCLCL(cRealEstate.getCLCL());
        newRealEstate.setCTXD_price(cRealEstate.getCTXD_price());
        newRealEstate.setCTXD_value(cRealEstate.getCTXD_value());
        newRealEstate.setPhoto(cRealEstate.getPhoto());
        newRealEstate.set_lat(cRealEstate.get_lat());
        newRealEstate.set_lng(cRealEstate.get_lng());
        newRealEstate.setPostStatus(0);
        newRealEstate = realEstateRepository.save(newRealEstate);
        return newRealEstate;
    }

    // Sửa RealEstate cho người dùng bình thường
    public CRealEstate updateRealEstateService(int id, CRealEstate cRealEstate) {
        Optional<CRealEstate> saveRealEstate = realEstateRepository.findById(id);
        if (saveRealEstate.isPresent()) {
            CRealEstate newRealEstate = saveRealEstate.get();
            newRealEstate.setTitle(cRealEstate.getTitle());
            newRealEstate.setType(cRealEstate.getType());
            newRealEstate.setRequest(cRealEstate.getRequest());
            newRealEstate.setProject_id(cRealEstate.getProject_id());
            newRealEstate.setProvince_id(cRealEstate.getProvince_id());
            newRealEstate.setDistrict_id(cRealEstate.getDistrict_id());
            newRealEstate.setWards_id(cRealEstate.getWards_id());
            newRealEstate.setStreet_id(cRealEstate.getStreet_id());
            newRealEstate.setAddress(cRealEstate.getAddress());
            newRealEstate.setCustomer_id(cRealEstate.getCustomer_id());
            newRealEstate.setPrice(cRealEstate.getPrice());
            newRealEstate.setPrice_min(cRealEstate.getPrice_min());
            newRealEstate.setPrice_time(cRealEstate.getPrice_time());
            newRealEstate.setAcreage(cRealEstate.getAcreage());
            newRealEstate.setDescription(cRealEstate.getDescription());
            newRealEstate.setTotal_floors(cRealEstate.getTotal_floors());
            newRealEstate.setNumber_floors(cRealEstate.getNumber_floors());
            newRealEstate.setBath(cRealEstate.getBath());
            newRealEstate.setApart_code(cRealEstate.getApart_code());
            newRealEstate.setWall_area(cRealEstate.getWall_area());
            newRealEstate.setBedroom(cRealEstate.getBedroom());
            newRealEstate.setBalcony(cRealEstate.getBalcony());
            newRealEstate.setLandscape_view(cRealEstate.getLandscape_view());
            newRealEstate.setApart_loca(cRealEstate.getApart_loca());
            newRealEstate.setApart_type(cRealEstate.getApart_type());
            newRealEstate.setFurniture_type(cRealEstate.getFurniture_type());
            newRealEstate.setPrice_rent(cRealEstate.getPrice_rent());
            newRealEstate.setReturn_rate(cRealEstate.getReturn_rate());
            newRealEstate.setLegal_doc(cRealEstate.getLegal_doc());
            newRealEstate.setDirection(cRealEstate.getDirection());
            newRealEstate.setWidth_y(cRealEstate.getWidth_y());
            newRealEstate.setLong_x(cRealEstate.getLong_x());
            newRealEstate.setStreet_house(cRealEstate.getStreet_house());
            newRealEstate.setFSBO(cRealEstate.getFSBO());
            newRealEstate.setView_num(cRealEstate.getView_num());
            newRealEstate.setUpdate_by(cRealEstate.getUpdate_by());
            newRealEstate.setShape(cRealEstate.getShape());
            newRealEstate.setDistance2facade(cRealEstate.getDistance2facade());
            newRealEstate.setAdjacent_alley_min_width(cRealEstate.getAdjacent_alley_min_width());
            newRealEstate.setAdjacent_road(cRealEstate.getAdjacent_road());
            newRealEstate.setAlley_min_width(cRealEstate.getAlley_min_width());
            newRealEstate.setFactor(cRealEstate.getFactor());
            newRealEstate.setStructure(cRealEstate.getStructure());
            newRealEstate.setDTSXD(cRealEstate.getDTSXD());
            newRealEstate.setCLCL(cRealEstate.getCLCL());
            newRealEstate.setCTXD_price(cRealEstate.getCTXD_price());
            newRealEstate.setCTXD_value(cRealEstate.getCTXD_value());
            newRealEstate.setPhoto(cRealEstate.getPhoto());
            newRealEstate.set_lat(cRealEstate.get_lat());
            newRealEstate.set_lng(cRealEstate.get_lng());
            newRealEstate.setPostStatus(0);
            newRealEstate = realEstateRepository.save(newRealEstate);
            return newRealEstate;
        } else {
            return null;
        }
    }

    // Sửa RealEstate trên trang admin
    public CRealEstate updateRealEstateAdminService(int id, CRealEstate cRealEstate) {
        Optional<CRealEstate> saveRealEstate = realEstateRepository.findById(id);
        if (saveRealEstate.isPresent()) {
            CRealEstate newRealEstate = saveRealEstate.get();
            newRealEstate.setTitle(cRealEstate.getTitle());
            newRealEstate.setType(cRealEstate.getType());
            newRealEstate.setRequest(cRealEstate.getRequest());
            newRealEstate.setProject_id(cRealEstate.getProject_id());
            newRealEstate.setProvince_id(cRealEstate.getProvince_id());
            newRealEstate.setDistrict_id(cRealEstate.getDistrict_id());
            newRealEstate.setWards_id(cRealEstate.getWards_id());
            newRealEstate.setStreet_id(cRealEstate.getStreet_id());
            newRealEstate.setAddress(cRealEstate.getAddress());
            newRealEstate.setCustomer_id(cRealEstate.getCustomer_id());
            newRealEstate.setPrice(cRealEstate.getPrice());
            newRealEstate.setPrice_min(cRealEstate.getPrice_min());
            newRealEstate.setPrice_time(cRealEstate.getPrice_time());
            newRealEstate.setAcreage(cRealEstate.getAcreage());
            newRealEstate.setDescription(cRealEstate.getDescription());
            newRealEstate.setTotal_floors(cRealEstate.getTotal_floors());
            newRealEstate.setNumber_floors(cRealEstate.getNumber_floors());
            newRealEstate.setBath(cRealEstate.getBath());
            newRealEstate.setApart_code(cRealEstate.getApart_code());
            newRealEstate.setWall_area(cRealEstate.getWall_area());
            newRealEstate.setBedroom(cRealEstate.getBedroom());
            newRealEstate.setBalcony(cRealEstate.getBalcony());
            newRealEstate.setLandscape_view(cRealEstate.getLandscape_view());
            newRealEstate.setApart_loca(cRealEstate.getApart_loca());
            newRealEstate.setApart_type(cRealEstate.getApart_type());
            newRealEstate.setFurniture_type(cRealEstate.getFurniture_type());
            newRealEstate.setPrice_rent(cRealEstate.getPrice_rent());
            newRealEstate.setReturn_rate(cRealEstate.getReturn_rate());
            newRealEstate.setLegal_doc(cRealEstate.getLegal_doc());
            newRealEstate.setDirection(cRealEstate.getDirection());
            newRealEstate.setWidth_y(cRealEstate.getWidth_y());
            newRealEstate.setLong_x(cRealEstate.getLong_x());
            newRealEstate.setStreet_house(cRealEstate.getStreet_house());
            newRealEstate.setFSBO(cRealEstate.getFSBO());
            newRealEstate.setView_num(cRealEstate.getView_num());
            newRealEstate.setUpdate_by(cRealEstate.getUpdate_by());
            newRealEstate.setShape(cRealEstate.getShape());
            newRealEstate.setDistance2facade(cRealEstate.getDistance2facade());
            newRealEstate.setAdjacent_alley_min_width(cRealEstate.getAdjacent_alley_min_width());
            newRealEstate.setAdjacent_road(cRealEstate.getAdjacent_road());
            newRealEstate.setAlley_min_width(cRealEstate.getAlley_min_width());
            newRealEstate.setFactor(cRealEstate.getFactor());
            newRealEstate.setStructure(cRealEstate.getStructure());
            newRealEstate.setDTSXD(cRealEstate.getDTSXD());
            newRealEstate.setCLCL(cRealEstate.getCLCL());
            newRealEstate.setCTXD_price(cRealEstate.getCTXD_price());
            newRealEstate.setCTXD_value(cRealEstate.getCTXD_value());
            newRealEstate.setPhoto(cRealEstate.getPhoto());
            newRealEstate.set_lat(cRealEstate.get_lat());
            newRealEstate.set_lng(cRealEstate.get_lng());
            newRealEstate.setPostStatus(cRealEstate.getPostStatus());
            newRealEstate.setReason(cRealEstate.getReason());

            newRealEstate = realEstateRepository.save(newRealEstate);
            return newRealEstate;
        } else {
            return null;
        }
    }

    // Cập nhật trạng thái bài đăng
    public CRealEstate updateRealEstateService(int id, int post_status) {
        Optional<CRealEstate> saveRealEstate = realEstateRepository.findById(id);
        if (saveRealEstate.isPresent()) {
            CRealEstate newRealEstate = saveRealEstate.get();
            newRealEstate.setPostStatus(post_status);
            newRealEstate = realEstateRepository.save(newRealEstate);
            return newRealEstate;
        } else {
            return null;
        }
    }

    // Xóa RealEstate theo Id
    public void deleteRealEstateService(int id) {
        realEstateRepository.deleteById(id);
    }

    // Truy vấn theo RealEstate ID
    // Output: 
    public List<CRealEstate> getRealEstateByIdAndStatusService(String page, String size, int id, int post_status) {
        Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
        List<CRealEstate> realEstateLst = realEstateRepository.findByIdAndByPoststatus(id, post_status,
                pageWithFiveElements);
        return realEstateLst;
    }

    // Truy vấn theo  ID của người tạo
    // Output: 
    public List<CRealEstate> getRealEstateByCreateId(int id) {
        List<CRealEstate> realEstateLst = realEstateRepository.findByCreateId(id);
        return realEstateLst;
    }

    //Truy vấn theo Id của khách hàng findByCustomerId
    public  List<CRealEstate> getRealEstateByCustomerId(int id) {
        List<CRealEstate> realEstateLst = realEstateRepository.findByCustomerId(id);
        return realEstateLst;
    }
}
