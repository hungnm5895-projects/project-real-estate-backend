package com.devcamp.bdsapi.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.bdsapi.Entity.CUtilities;
import com.devcamp.bdsapi.Respository.UtilitiesRepository;

@Service
public class UtilitiesService {
    @Autowired
    UtilitiesRepository utilitiesRepository;

    // Truy vấn toàn bộ Design Unit
    public List<CUtilities> getAllUtilitiesService() {
        List<CUtilities> lstUti = new ArrayList<>();
        utilitiesRepository.findAll().forEach(lstUti::add);
        return lstUti;
    }

    // Truy vấn Utilities theo ID
    // Output: Optional Utilities
    public Optional<CUtilities> getUtilitiesByIdService(int id) {
        Optional<CUtilities> UtilOptional = utilitiesRepository.findById(id);
        return UtilOptional;
    }

    // Thêm mới Utilities
    public CUtilities createUtilitiesService(CUtilities cUtilities) {
        CUtilities newUtilities = new CUtilities();
        newUtilities.setDescription(cUtilities.getDescription());
        newUtilities.setName(cUtilities.getName());
        newUtilities.setPhoto(cUtilities.getPhoto());
        newUtilities = utilitiesRepository.save(newUtilities);
        return newUtilities;
    }

    // Sửa Utilities
    public CUtilities updateUtilitiesService(int id, CUtilities cUtilities) {
        Optional<CUtilities> saveUtilitiesOptional = utilitiesRepository.findById(id);
        if (saveUtilitiesOptional.isPresent()) {
            CUtilities newUtilities = saveUtilitiesOptional.get();
            newUtilities.setDescription(cUtilities.getDescription());
            newUtilities.setName(cUtilities.getName());
            newUtilities.setPhoto(cUtilities.getPhoto());
            newUtilities = utilitiesRepository.save(newUtilities);
            return newUtilities;
        } else {
            return null;
        }
    }

    // Xóa design Unit theo Id
    public void deleteUtilitiesService(int id) {
        utilitiesRepository.deleteById(id);
    }
}
