package com.devcamp.bdsapi.Service;

import com.devcamp.bdsapi.Entity.Token;

public interface TokenService {
    
    Token createToken(Token token);

    Token findByToken(String token);
}
