package com.devcamp.bdsapi.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.bdsapi.Entity.CDistrict;
import com.devcamp.bdsapi.Entity.CWard;
import com.devcamp.bdsapi.Respository.DistrictRepository;
import com.devcamp.bdsapi.Respository.WardRepository;

@Service
public class WardService {
    @Autowired
    WardRepository wardRepository;

    @Autowired
    DistrictRepository districtRepository;

    public Set<CWard> getWardsByDistrictIdService(int districtId){
        Optional<CDistrict> districtOptional = districtRepository.findById(districtId);
        if (districtOptional.isPresent()) {
            return districtOptional.get().getWards();
        } else {
            return null;
        }
    }

    public List<CWard> getAllWardService(){
        return wardRepository.findAll();
    }

    //Lấy district theo id
    public Optional<CWard> getWardByIdService(int id){
        return wardRepository.findById(id);
    }
}
