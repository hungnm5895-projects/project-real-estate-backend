package com.devcamp.bdsapi.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.bdsapi.Entity.CContructionContractor;
import com.devcamp.bdsapi.Respository.ContructionContractorRepositopy;

@Service
public class ContructionContractorService {
    @Autowired
    ContructionContractorRepositopy contractorRepositopy;

    // Truy vấn toàn bộ Design Unit
    public List<CContructionContractor> getAllContructionService() {
        List<CContructionContractor> lstContruct = new ArrayList<>();
        contractorRepositopy.findAll().forEach(lstContruct::add);
        return lstContruct;
    }

    // Truy vấn Design Unit theo ID
    // Output: Optional Design Unit
    public Optional<CContructionContractor> getContructionContructorServiceByIdService(int id) {
        Optional<CContructionContractor> conTructionOptional = contractorRepositopy.findById(id);
        return conTructionOptional;
    }

    // Thêm mới Design Unit
    public CContructionContractor createContructionContractorService(CContructionContractor cContructionContractor) {
        CContructionContractor newContructionContractor = new CContructionContractor();
        newContructionContractor.setName(cContructionContractor.getName());
        newContructionContractor.setDescription(cContructionContractor.getDescription());
        newContructionContractor.setProjects(cContructionContractor.getProjects());
        newContructionContractor.setAddress(cContructionContractor.getAddress());
        newContructionContractor.setPhone(cContructionContractor.getPhone());
        newContructionContractor.setPhone2(cContructionContractor.getPhone2());
        newContructionContractor.setFax(cContructionContractor.getFax());
        newContructionContractor.setEmail(cContructionContractor.getEmail());
        newContructionContractor.setWebsite(cContructionContractor.getWebsite());
        newContructionContractor.setNote(cContructionContractor.getNote());
        newContructionContractor = contractorRepositopy.save(newContructionContractor);
        return newContructionContractor;
    }

    // Sửa Design Unit
    public CContructionContractor updateContructionContructorService(int id, CContructionContractor cContructionContractor) {
        Optional<CContructionContractor> saveContructOptional = contractorRepositopy.findById(id);
        if (saveContructOptional.isPresent()) {
            CContructionContractor newContructionContractor = saveContructOptional.get();
            newContructionContractor.setName(cContructionContractor.getName());
            newContructionContractor.setDescription(cContructionContractor.getDescription());
            newContructionContractor.setProjects(cContructionContractor.getProjects());
            newContructionContractor.setAddress(cContructionContractor.getAddress());
            newContructionContractor.setPhone(cContructionContractor.getPhone());
            newContructionContractor.setPhone2(cContructionContractor.getPhone2());
            newContructionContractor.setFax(cContructionContractor.getFax());
            newContructionContractor.setEmail(cContructionContractor.getEmail());
            newContructionContractor.setWebsite(cContructionContractor.getWebsite());
            newContructionContractor.setNote(cContructionContractor.getNote());
            newContructionContractor = contractorRepositopy.save(newContructionContractor);
            return newContructionContractor;
        } else {
            return null;
        }
    }

    //Xóa design Unit theo Id
    public void deleteContructionContractorService(int id) {
        contractorRepositopy.deleteById(id);
    }
}
