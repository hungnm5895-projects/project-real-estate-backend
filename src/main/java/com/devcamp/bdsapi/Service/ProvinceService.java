package com.devcamp.bdsapi.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.bdsapi.Entity.CProvince;
import com.devcamp.bdsapi.Respository.ProvinceRepository;

@Service
public class ProvinceService {
    @Autowired
    ProvinceRepository provinceRepository;

    public List<CProvince> getAllProvinceService(){
        List<CProvince> lstProvinces = new ArrayList<>();
        provinceRepository.findAll().forEach(lstProvinces::add);
        return lstProvinces;
    }

    public Optional<CProvince> getProvinceByIdService(int id){
        return provinceRepository.findById(id);
    }
}
