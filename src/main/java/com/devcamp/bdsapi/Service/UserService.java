package com.devcamp.bdsapi.Service;

import java.util.List;

import com.devcamp.bdsapi.Entity.User;

public interface UserService {
    User createUser(User user, String keyRole);

    com.devcamp.bdsapi.Sercurity.UserPrincipal findByUsername(String username);

    User createUserIndex(User user, String keyRole);

    //Lấy danh sách user
    List<User> getAllUserService();

    User lockUser(long id);

    User updateUser(User user, long id, String roleKey); 
}
