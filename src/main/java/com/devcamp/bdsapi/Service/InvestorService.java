package com.devcamp.bdsapi.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.bdsapi.Entity.CInvestor;
import com.devcamp.bdsapi.Respository.InvestorRepository;

@Service
public class InvestorService {
    @Autowired
    InvestorRepository investorRepository;

    // Truy vấn toàn bộ nhà đầu tư
    public List<CInvestor> getAllInvestorService() {
        List<CInvestor> lstInvestors = new ArrayList<>();
        investorRepository.findAll().forEach(lstInvestors::add);
        return lstInvestors;
    }


    // Truy vấn Design Unit theo ID
    // Output: Optional Design Unit
    public Optional<CInvestor> getInvestorByIdService(int id) {
        Optional<CInvestor> designUniteOptional = investorRepository.findById(id);
        return designUniteOptional;
    }

    // Thêm mới Design Unit
    public CInvestor createInvestorService(CInvestor cInvestor) {
        CInvestor newInvestor = new CInvestor();
        newInvestor.setName(cInvestor.getName());
        newInvestor.setDescription(cInvestor.getDescription());
        newInvestor.setProjects(cInvestor.getProjects());
        newInvestor.setAddress(cInvestor.getAddress());
        newInvestor.setPhone(cInvestor.getPhone());
        newInvestor.setPhone2(cInvestor.getPhone2());
        newInvestor.setFax(cInvestor.getFax());
        newInvestor.setEmail(cInvestor.getEmail());
        newInvestor.setWebsite(cInvestor.getWebsite());
        newInvestor.setNote(cInvestor.getNote());
        newInvestor = investorRepository.save(newInvestor);
        return newInvestor;
    }

    // Sửa Investor
    public CInvestor updateInvestorService(int id, CInvestor cInvestor) {
        Optional<CInvestor> saveDesignUnitOptional = investorRepository.findById(id);
        if (saveDesignUnitOptional.isPresent()) {
            CInvestor newInvestor = saveDesignUnitOptional.get();
            newInvestor.setName(cInvestor.getName());
            newInvestor.setDescription(cInvestor.getDescription());
            newInvestor.setProjects(cInvestor.getProjects());
            newInvestor.setAddress(cInvestor.getAddress());
            newInvestor.setPhone(cInvestor.getPhone());
            newInvestor.setPhone2(cInvestor.getPhone2());
            newInvestor.setFax(cInvestor.getFax());
            newInvestor.setEmail(cInvestor.getEmail());
            newInvestor.setWebsite(cInvestor.getWebsite());
            newInvestor.setNote(cInvestor.getNote());
            newInvestor = investorRepository.save(newInvestor);
            return newInvestor;
        } else {
            return null;
        }
    }

    //Xóa design Unit theo Id
    public void deleteInvestorService(int id) {
        investorRepository.deleteById(id);
    }
}
