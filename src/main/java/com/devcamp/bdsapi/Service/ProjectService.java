package com.devcamp.bdsapi.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.bdsapi.Entity.CProject;
import com.devcamp.bdsapi.Respository.ProjectRepository;

@Service
public class ProjectService {
    @Autowired
    ProjectRepository projectRepository;

    // Truy vấn toàn bộ Project
    public List<CProject> getAllProjectService() {
        List<CProject> lstProjects = new ArrayList<>();
        projectRepository.findAll().forEach(lstProjects::add);
        return lstProjects;
    }

    // Truy vấn theo Project ID
    // Output: Optional
    public Optional<CProject> getProjectByIdService(int id) {
        Optional<CProject> projectOptional = projectRepository.findById(id);
        return projectOptional;
    }

    // Thêm mới Project
    public CProject createProjectService(CProject cProject) {
        CProject newProjet = new CProject();
        newProjet.setName(cProject.getName());
        newProjet.setProvinceId(cProject.getProvinceId());
        newProjet.setDistrictId(cProject.getDistrictId());
        newProjet.setWardId(cProject.getWardId());
        newProjet.setStreetId(cProject.getStreetId());
        newProjet.setAddress(cProject.getAddress());
        newProjet.setDescription(cProject.getDescription());
        newProjet.setAcreage(cProject.getAcreage());
        newProjet.setConstructArea(cProject.getConstructArea());
        newProjet.setNumBlock(cProject.getNumBlock());
        newProjet.setNumApartment(cProject.getNumApartment());
        newProjet.setApartmenttArea(cProject.getApartmenttArea());
        newProjet.setInvestor(cProject.getInvestor());
        newProjet.setConstructionContractor(cProject.getConstructionContractor());
        newProjet.setDesignUnit(cProject.getDesignUnit());
        newProjet.setUtilities(cProject.getUtilities());
        newProjet.setRegionLink(cProject.getRegionLink());
        newProjet.setPhoto(cProject.getPhoto());
        newProjet.setLat(cProject.getLat());
        newProjet.setLng(cProject.getLng());

        newProjet = projectRepository.save(newProjet);
        return newProjet;
    }

    // Sửa Project
    public CProject updateProjectService(int id, CProject cProject) {
        Optional<CProject> sOptional = projectRepository.findById(id);
        if (sOptional.isPresent()) {
            CProject newProjet = sOptional.get();
            newProjet.setName(cProject.getName());
            newProjet.setProvinceId(cProject.getProvinceId());
            newProjet.setDistrictId(cProject.getDistrictId());
            newProjet.setWardId(cProject.getWardId());
            newProjet.setStreetId(cProject.getStreetId());
            newProjet.setAddress(cProject.getAddress());
            newProjet.setDescription(cProject.getDescription());
            newProjet.setAcreage(cProject.getAcreage());
            newProjet.setConstructArea(cProject.getConstructArea());
            newProjet.setNumBlock(cProject.getNumBlock());
            newProjet.setNumFloors(cProject.getNumFloors());
            newProjet.setNumApartment(cProject.getNumApartment());
            newProjet.setApartmenttArea(cProject.getApartmenttArea());
            newProjet.setInvestor(cProject.getInvestor());
            newProjet.setConstructionContractor(cProject.getConstructionContractor());
            newProjet.setDesignUnit(cProject.getDesignUnit());
            newProjet.setUtilities(cProject.getUtilities());
            newProjet.setRegionLink(cProject.getRegionLink());
            newProjet.setPhoto(cProject.getPhoto());
            newProjet.setLat(cProject.getLat());
            newProjet.setLng(cProject.getLng());
            newProjet = projectRepository.save(newProjet);
            return newProjet;
        } else {
            return null;
        }
    }

    // Xóa theo Id
    public void deleteProjectService(int id) {
        projectRepository.deleteById(id);
    }

    // API truy vấn danh sách Project theo district id
    public List<CProject> getProjectListByDistrictId(int district_id) {
        return projectRepository.findByDistrictId(district_id);
    }
}
