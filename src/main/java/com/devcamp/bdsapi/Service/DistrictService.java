package com.devcamp.bdsapi.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.bdsapi.Entity.CDistrict;
import com.devcamp.bdsapi.Entity.CProvince;
import com.devcamp.bdsapi.Respository.DistrictRepository;
import com.devcamp.bdsapi.Respository.ProvinceRepository;

@Service
public class DistrictService {
    @Autowired
    DistrictRepository districtRepository;

    @Autowired
    ProvinceRepository provinceRepository;

    //hàm truyền vào Province Id và trả ra list Districts
    public Set<CDistrict> getDistrictByProvinceIdService(int provinceId){
        Optional<CProvince> provOptional = provinceRepository.findById(provinceId);
        if (provOptional.isPresent()) {
            return provOptional.get().getDistricts();
        } else {
            return null;
        }
    }

    //Service Lấy toàn bộ district
    public List<CDistrict> getAllDistrictsService(){
        return districtRepository.findAll();
    }

    //Lấy district theo id
    public Optional<CDistrict> getDistrictByIdService(int id){
        return districtRepository.findById(id);
    }
}
