package com.devcamp.bdsapi.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.bdsapi.Entity.CRegionLink;
import com.devcamp.bdsapi.Respository.RegionLinkRepository;

@Service
public class RegionLinkService {
    @Autowired
    RegionLinkRepository regionLinkRepository;

    // Truy vấn toàn bộ Design Unit
    public List<CRegionLink> getAllRegionLinkService() {
        List<CRegionLink> lstRe = new ArrayList<>();
        regionLinkRepository.findAll().forEach(lstRe::add);
        return lstRe;
    }

    // Truy vấn Design Unit theo ID
    // Output: Optional Design Unit
    public Optional<CRegionLink> getRegionLinkByIdService(int id) {
        Optional<CRegionLink> designUniteOptional = regionLinkRepository.findById(id);
        return designUniteOptional;
    }

    public CRegionLink updateRegionLinkService(int id, CRegionLink cRegionLink) {
        Optional<CRegionLink> saveRegionLinkOptional = regionLinkRepository.findById(id);
        if (saveRegionLinkOptional.isPresent()) {
            CRegionLink newRegionLink = saveRegionLinkOptional.get();
            newRegionLink.setAddress(cRegionLink.getAddress());
            newRegionLink.setDescription(cRegionLink.getDescription());
            newRegionLink.setPhoto(cRegionLink.getPhoto());
            newRegionLink.setName(cRegionLink.getName());
            newRegionLink.set_lat(cRegionLink.get_lat());
            newRegionLink.set_lat(cRegionLink.get_lng());
            newRegionLink = regionLinkRepository.save(newRegionLink);
            return newRegionLink;
        } else {
            return null;
        }
    }

    //Xóa design Unit theo Id
    public void deleteRegionLinkService(int id) {
        regionLinkRepository.deleteById(id);
    }
}
