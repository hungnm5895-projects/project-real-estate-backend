package com.devcamp.bdsapi.Model;

import java.util.List;

import com.devcamp.bdsapi.Entity.CRealEstate;

public class MRealEstateResponse {
    private List<CRealEstate> data;
    private int count;

    public MRealEstateResponse(List<CRealEstate> lstEstate, int countData) {
        this.data = lstEstate;
        this.count = countData;
    }

    public List<CRealEstate> getData() {
        return data;
    }

    public void setData(List<CRealEstate> data) {
        this.data = data;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

}
