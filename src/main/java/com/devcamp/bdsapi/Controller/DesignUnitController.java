package com.devcamp.bdsapi.Controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bdsapi.Entity.CDesignUnit;
import com.devcamp.bdsapi.Service.DesignUnitService;

@RestController
@CrossOrigin
public class DesignUnitController {
    @Autowired
    DesignUnitService designUnitService;

    @GetMapping("/designUnits")
    public ResponseEntity<Object> getAllDesignUnits() {
        try {
            List<CDesignUnit> lstDes = designUnitService.getAllDesginUnitService();
            return ResponseEntity.status(HttpStatus.OK).body(lstDes);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau!  ");
        }
    }

    @GetMapping("/designUnits/{id}")
    public ResponseEntity<Object> getDesignUnitById(@PathVariable(value = "id") int id) {
        try {
            Optional<CDesignUnit> desOptional = designUnitService.getDesignServiceByIdService(id);
            if (desOptional.isPresent()) {
                return ResponseEntity.status(HttpStatus.OK).body(desOptional.get());
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    @PostMapping("/designUnits")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> createDesignUnit(@Valid @RequestBody CDesignUnit cDesignUnit) {
        try {
            CDesignUnit newDesignUnit = designUnitService.createDesignUnitService(cDesignUnit);
            return ResponseEntity.status(HttpStatus.CREATED).body(newDesignUnit);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau!  ");
        }
    }

    @PutMapping("/designUnits/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateDesignUnit(@PathVariable(name = "id") int id,
            @RequestBody CDesignUnit cDesignUnit) {
        try {
            CDesignUnit newDesignUnit = designUnitService.updateDesignUnitService(id, cDesignUnit);
            if (newDesignUnit != null) {
                return ResponseEntity.status(HttpStatus.OK).body(newDesignUnit);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau!  ");
        }
    }

    @DeleteMapping("/designUnits/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> deleteDesignUnit(@PathVariable(name = "id") int id) {
        try {
            designUnitService.deleteDesignUnitService(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau!  ");
        }
    }
}
