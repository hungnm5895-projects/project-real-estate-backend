package com.devcamp.bdsapi.Controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import com.devcamp.bdsapi.Entity.CInvestor;
import com.devcamp.bdsapi.Service.InvestorService;

@RestController
@CrossOrigin
public class InvestorController {
    @Autowired
    InvestorService investorService;

    @GetMapping("/investors")
    public ResponseEntity<Object> getAllDesignUnits() {
        try {
            List<CInvestor> lstDes = investorService.getAllInvestorService();
            return ResponseEntity.status(HttpStatus.OK).body(lstDes);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    @GetMapping("/investors/{id}")
    public ResponseEntity<Object> getDesignUnitById(@PathVariable(value = "id") int id) {
        try {
            Optional<CInvestor> desOptional = investorService.getInvestorByIdService(id);
            if (desOptional.isPresent()) {
                return ResponseEntity.status(HttpStatus.OK).body(desOptional.get());
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    @PostMapping("/investors")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> createDesignUnit(@Valid @RequestBody CInvestor cInvestor) {
        try {
            CInvestor newInvestor = investorService.createInvestorService(cInvestor);
            return ResponseEntity.status(HttpStatus.CREATED).body(newInvestor);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/investors/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateDesignUnit(@PathVariable(name = "id") int id, @Valid @RequestBody CInvestor cInvestor) {
        try {
            CInvestor newInvestor =  investorService.updateInvestorService(id, cInvestor);;
            if (newInvestor != null) {
                return ResponseEntity.status(HttpStatus.OK).body(newInvestor);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    @DeleteMapping("/investors/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> deleteDesignUnit(@PathVariable(name = "id") int id) {
        try {
            investorService.deleteInvestorService(id);;
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create Staff: " + e.getCause().getCause().getMessage());
        }
    }
}
