package com.devcamp.bdsapi.Controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import com.devcamp.bdsapi.Entity.CContructionContractor;
import com.devcamp.bdsapi.Service.ContructionContractorService;

@RestController
@CrossOrigin
public class ContructionContructorController {
    @Autowired
    ContructionContractorService contructionContractorService;

    // Api Lấy toàn bộ contruction Contructors
    @GetMapping("/contructionContructors")
    public ResponseEntity<Object> getAllContructionContructor() {
        try {
            List<CContructionContractor> lstCon = contructionContractorService.getAllContructionService();
            return ResponseEntity.status(HttpStatus.OK).body(lstCon);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau!");
        }
    }

    @GetMapping("/contructionContructors/{id}")
    public ResponseEntity<Object> getContructionContructorById(@PathVariable(value = "id") int id) {
        try {
            Optional<CContructionContractor> contructionOptional = contructionContractorService
                    .getContructionContructorServiceByIdService(id);
            if (contructionOptional.isPresent()) {
                return ResponseEntity.status(HttpStatus.OK).body(contructionOptional.get());
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau!");
        }
    }

    @PostMapping("/contructionContructors")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> createContructionContructor(
            @Valid @RequestBody CContructionContractor cContructionContractor) {
        try {
            CContructionContractor newContruction = contructionContractorService
                    .createContructionContractorService(cContructionContractor);
            return ResponseEntity.status(HttpStatus.CREATED).body(newContruction);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau!");
        }
    }

    @PutMapping("/contructionContructors/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateContructionContructor(@Valid @PathVariable(name = "id") int id,
            @RequestBody CContructionContractor cContructionContractor) {
        try {
            CContructionContractor newCContructionContractor = contructionContractorService
                    .updateContructionContructorService(id, cContructionContractor);
            if (newCContructionContractor != null) {
                return ResponseEntity.status(HttpStatus.OK).body(newCContructionContractor);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    @DeleteMapping("/contructionContructors/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> deleteContructionContructor(@PathVariable(name = "id") int id) {
        try {
            contructionContractorService.deleteContructionContractorService(id);
            ;
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }
}
