package com.devcamp.bdsapi.Controller;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bdsapi.Entity.CWard;
import com.devcamp.bdsapi.Service.WardService;

@RestController
@CrossOrigin
public class WardController {
    @Autowired
    WardService wardService;

    @GetMapping("/districts/{districtId}/wards")
    public ResponseEntity<Object> getWardsByDistrictId(@PathVariable(value = "districtId")int districtId){
        try {
            Set<CWard> lstWards = wardService.getWardsByDistrictIdService(districtId);
            return ResponseEntity.status(HttpStatus.OK).body(lstWards);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to load Wards List: " + e.getCause().getCause().getMessage());
        }
    }

    @GetMapping("/wards")
    public ResponseEntity<Object> getAllWard(){
        try {
            List<CWard> lstWards = wardService.getAllWardService();
            return ResponseEntity.status(HttpStatus.OK).body(lstWards);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to load Wards List: " + e.getCause().getCause().getMessage());
        }
    }

    @GetMapping("/wards/{id}")
    public ResponseEntity<Object> getWardById(@PathVariable(value = "id")int id) {
        try {
            Optional<CWard> distrOptional = wardService.getWardByIdService(id);
            if (distrOptional.isPresent()) {
                return ResponseEntity.status(HttpStatus.OK).body(distrOptional.get());
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }
}
