package com.devcamp.bdsapi.Controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bdsapi.Entity.CEmployees;
import com.devcamp.bdsapi.Service.EmployeeService;

@RestController
@CrossOrigin
public class EmployeesController {
    @Autowired
    EmployeeService employeeService;

    @GetMapping("/employees")
    public ResponseEntity<Object> getAllEmployees() {
        try {
            List<CEmployees> lstEmployees = employeeService.getAllEmployeesService();
            return ResponseEntity.status(HttpStatus.OK).body(lstEmployees);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau!  ");
        }
    }

    // Api truyền ID trả về chi tiết customer
    @GetMapping("/employees/{id}")
    public ResponseEntity<Object> getEmployeesById(@PathVariable(value = "id") int id) {
        try {
            Optional<CEmployees> employeesOptional = employeeService.getEmployeesByIdService(id);
            if (employeesOptional.isPresent()) {
                return ResponseEntity.status(HttpStatus.OK).body(employeesOptional.get());
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau!  ");
        }
    }

    // API tạo mới Employees
    @PostMapping("/employees")
    public ResponseEntity<Object> createEmployees(@Valid @RequestBody CEmployees cEmployees) {
        try {
            CEmployees newEmployees = employeeService.createEmployeesService(cEmployees);
            return ResponseEntity.status(HttpStatus.CREATED).body(newEmployees);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau!  ");
        }
    }

    // API cập nhật thông tin Employees
    @PutMapping("/employees/{id}")
    public ResponseEntity<Object> updateEmployees(@PathVariable(name = "id") int id,
            @Valid @RequestBody CEmployees cEmployees) {
        try {
            CEmployees saveEmployees = employeeService.updateEmployeesService(id, cEmployees);
            if (saveEmployees != null) {
                return ResponseEntity.status(HttpStatus.OK).body(saveEmployees);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau!  ");
        }
    }

    // API xóa Employees
    @DeleteMapping("/employees/{id}")
    public ResponseEntity<Object> deleteEmployees(@PathVariable(name = "id") int id) {
        try {
            employeeService.deleteEmployeesService(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau!  ");
        }
    }
}
