package com.devcamp.bdsapi.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.bdsapi.Entity.CSubscription;
import com.devcamp.bdsapi.Service.SubscriptionService;

@RestController
@CrossOrigin
public class SubscriptionController {
    @Autowired
    SubscriptionService subscriptionService;

    @GetMapping("/subscriptions")
    public ResponseEntity<Object> getAllSubscription() {
        try {
            List<CSubscription> lstSubscriptions = subscriptionService.getAllSubscriptionService();
            return ResponseEntity.status(HttpStatus.OK).body(lstSubscriptions);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    // Api truyền ID trả về chi tiết subscription
    @GetMapping("/subscriptions/{id}")
    public ResponseEntity<Object> getSubscriptionById(@PathVariable(value = "id") int id) {
        try {
            Optional<CSubscription> subOptional = subscriptionService.getSubscriptionByIdService(id);
            if (subOptional.isPresent()) {
                return ResponseEntity.status(HttpStatus.OK).body(subOptional.get());
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    // API tạo mới subscriptions
    @PostMapping("/subscriptions")
    public ResponseEntity<Object> createCustomer(@RequestBody CSubscription cSubscription) {
        try {
            CSubscription newSubscription = subscriptionService.createSubscriptionService(cSubscription);
            return ResponseEntity.status(HttpStatus.CREATED).body(newSubscription);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    // API cập nhật thông tin subscriptions
    @PutMapping("/subscriptions/{id}")
    public ResponseEntity<Object> updateSubscription(@PathVariable(name = "id") int id,
            @RequestBody CSubscription cSubscription) {
        try {
            CSubscription saveSubscription = subscriptionService.updateSubscriptionService(id, cSubscription);
            if (saveSubscription != null) {
                return ResponseEntity.status(HttpStatus.OK).body(saveSubscription);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    // API xóa Subscription
    @DeleteMapping("/subscriptions/{id}")
    public ResponseEntity<Object> deleteSubscription(@PathVariable(name = "id") int id) {
        try {
            subscriptionService.deleteSubscriptionService(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed : " + e.getCause().getCause().getMessage());
        }
    }
}
