package com.devcamp.bdsapi.Controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import com.devcamp.bdsapi.Entity.CUtilities;
import com.devcamp.bdsapi.Service.UtilitiesService;

@RestController
@CrossOrigin
public class UtilitiesController {
    @Autowired
    UtilitiesService utilitiesService;

    @GetMapping("/utilities")
    public ResponseEntity<Object> getAllUtilities() {
        try {
            List<CUtilities> lstUti = utilitiesService.getAllUtilitiesService();
            return ResponseEntity.status(HttpStatus.OK).body(lstUti);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    //Api truyền ID trả về chi tiết tiện ích
    @GetMapping("/utilities/{id}")
    public ResponseEntity<Object> getDesignUnitById(@PathVariable(value = "id") int id) {
        try {
            Optional<CUtilities> utilOptional = utilitiesService.getUtilitiesByIdService(id);
            if (utilOptional.isPresent()) {
                return ResponseEntity.status(HttpStatus.OK).body(utilOptional.get());
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    //API tạo mới tiện ích
    @PostMapping("/utilities")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> createDesignUnit(@Valid @RequestBody CUtilities cUtilities) {
        try {
            CUtilities newUtilities = utilitiesService.createUtilitiesService(cUtilities);
            return ResponseEntity.status(HttpStatus.CREATED).body(newUtilities);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    //API cập nhật thông tin tiện ích
    @PutMapping("/utilities/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateDesignUnit(@PathVariable(name = "id") int id, @Valid @RequestBody CUtilities cUtilities) {
        try {
            CUtilities newUtil =  utilitiesService.updateUtilitiesService(id, cUtilities);
            if (newUtil != null) {
                return ResponseEntity.status(HttpStatus.OK).body(newUtil);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    //API xóa tiện ích
    @DeleteMapping("/utilities/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> deleteDesignUnit(@PathVariable(name = "id") int id) {
        try {
            utilitiesService.deleteUtilitiesService(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed : " + e.getCause().getCause().getMessage());
        }
    }
}
