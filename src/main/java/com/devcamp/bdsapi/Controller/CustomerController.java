package com.devcamp.bdsapi.Controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bdsapi.Entity.CCustomer;
import com.devcamp.bdsapi.Service.CustomerService;

@RestController
@CrossOrigin
public class CustomerController {
    @Autowired
    CustomerService customerService;

    @GetMapping("/customers")
    public ResponseEntity<Object> getAllCustomer() {
        try {
            List<CCustomer> lstCustomers = customerService.getAllCustomerService();
            return ResponseEntity.status(HttpStatus.OK).body(lstCustomers);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    // Api truyền ID trả về chi tiết customer
    @GetMapping("/customers/{id}")
    public ResponseEntity<Object> getCustomerById(@PathVariable(value = "id") int id) {
        try {
            Optional<CCustomer> customerOptional = customerService.getCustomerByIdService(id);
            if (customerOptional.isPresent()) {
                return ResponseEntity.status(HttpStatus.OK).body(customerOptional.get());
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    // API tạo mới customers
    @PostMapping("/customers")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> createCustomer(@Valid @RequestBody CCustomer cCustomer) {
        try {
            CCustomer newCustomer = customerService.createCustomerService(cCustomer);
            return ResponseEntity.status(HttpStatus.CREATED).body(newCustomer);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    // API cập nhật thông tin customers
    @PutMapping("/customers/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateCustomer(@PathVariable(name = "id") int id,
            @Valid @RequestBody CCustomer cCustomer) {
        try {
            CCustomer saveCustomer = customerService.updateCustomerService(id, cCustomer);
            if (saveCustomer != null) {
                return ResponseEntity.status(HttpStatus.OK).body(saveCustomer);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau! ");
        }
    }

    // API xóa customers
    @DeleteMapping("/customers/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> deleteProject(@PathVariable(name = "id") int id) {
        try {
            customerService.deleteCustomerService(id);
            ;
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau!  ");
        }
    }
}
