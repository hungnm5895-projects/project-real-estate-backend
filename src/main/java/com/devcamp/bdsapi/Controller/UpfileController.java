package com.devcamp.bdsapi.Controller;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.*;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@CrossOrigin
public class UpfileController {

    @PostMapping(value = "/upfile")
    public ResponseEntity<Object> uploadFile(@RequestParam("image") MultipartFile multipartFile) {
        try {
            String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
            String uploadDir = "uploads/";
            FileUploadUtil.saveFile(uploadDir, getFileName(fileName), multipartFile);
            return ResponseEntity.status(HttpStatus.OK).body(getFileName(fileName));

        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.OK).body("");
        }

    }

    @PostMapping(value = "/upfile-multi")
    public ResponseEntity<Object> uploadMultiFile(@RequestParam("image") MultipartFile[] multipartFile) {
        try {
            String regionPhoto = "";
            for (MultipartFile file : multipartFile) {
                String fileName = StringUtils.cleanPath(file.getOriginalFilename());
                String uploadDir = "uploads/";
                FileUploadUtil.saveFile(uploadDir, fileName, file);
                regionPhoto = regionPhoto + ";" + fileName;
            }
            return ResponseEntity.status(HttpStatus.OK).body(regionPhoto);

        } catch (IOException e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }

    }

    private static String getFileName(String file) {
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("ddMMyyyyHHmmss");
        LocalDateTime myDateObj = LocalDateTime.now();
        String formattedDate = myDateObj.format(myFormatObj);
        return formattedDate + file;
    }
}
