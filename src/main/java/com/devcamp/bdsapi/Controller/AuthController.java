package com.devcamp.bdsapi.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bdsapi.Entity.Token;
import com.devcamp.bdsapi.Entity.User;
import com.devcamp.bdsapi.Model.MLoginResponse;
import com.devcamp.bdsapi.Respository.RoleRespository;
import com.devcamp.bdsapi.Respository.UserRepository;
import com.devcamp.bdsapi.Sercurity.JwtUtil;
import com.devcamp.bdsapi.Sercurity.UserPrincipal;
import com.devcamp.bdsapi.Service.TokenService;
import com.devcamp.bdsapi.Service.UserService;
import org.springframework.web.bind.annotation.GetMapping;

@RestController
@CrossOrigin
public class AuthController {
    @Autowired
    private UserService userService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    RoleRespository roleRespository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    private JwtUtil jwtUtil;

    // API đăng ký tài khoản tại Landing Page
    @PostMapping("/register/{keyRole}")
    public ResponseEntity<Object> register(@RequestBody User user, @PathVariable(name = "keyRole") String keyRole) {
        try {
            // Check Vai trò truyền vào phải có tồn tại
            if (roleRespository.findByRoleKey(keyRole) == null) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Vai trò không hợp lệ, vui lòng thử lại");
            }

            // Chức năng Không cho đăng ký ADMIN và MANAGER
            if (keyRole.equals("ROLE_ADMIN") || keyRole.equals("ROLE_MANAGER")) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Vai trò không hợp lệ, vui lòng thử lại");
            }

            // User không được trùng
            if (userRepository.existsByUsername(user.getUsername())) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Tài khoản đã tồn tại, vui lòng sử dụng tài khoản khác");
            }

            // Email không được trùng
            if (userRepository.existsByEmail(user.getEmail())) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Email đã được đăng ký, vui lòng sử dụng tài khoản khác");
            }

            // Phải nhập câu trả lời bí mật
            if (user.getSecretAnswer() == null || user.getSecretAnswer() == "") {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Bạn cần nhập câu trả lời bí mật, vui lòng thử lại");
            }

            return ResponseEntity.status(HttpStatus.OK).body(userService.createUser(user, keyRole));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Có lỗi xảy ra, vui lòng thử lại.");
        }
    }

    // API đăng ký tài khoản tại trang Admin
    @PostMapping("/createUser/{keyRole}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> registerIndex(@RequestBody User user,
            @PathVariable(name = "keyRole") String keyRole) {
        try {
            // Check Vai trò truyền vào phải có tồn tại
            if (roleRespository.findByRoleKey(keyRole) == null) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Vai trò không hợp lệ, vui lòng thử lại");
            }

            // User không được trùng
            if (userRepository.existsByUsername(user.getUsername())) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Tài khoản đã tồn tại, vui lòng sử dụng tài khoản khác");
            }

            // Email không được trùng
            if (userRepository.existsByEmail(user.getEmail())) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Email đã được đăng ký, vui lòng sử dụng tài khoản khác");
            }

            // Phải nhập câu trả lời bí mật
            if (user.getSecretAnswer() == null || user.getSecretAnswer() == "") {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Bạn cần nhập câu trả lời bí mật, vui lòng thử lại");
            }
            return ResponseEntity.status(HttpStatus.OK).body(userService.createUser(user, keyRole));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Có lỗi xảy ra, vui lòng thử lại.");
        }
    }

    // API đăng nhập
    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody User user) {
        UserPrincipal userPrincipal = userService.findByUsername(user.getUsername());
        if (null == user || !new BCryptPasswordEncoder().matches(user.getPassword(), userPrincipal.getPassword())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("tài khoản hoặc mật khẩu không chính xác");
        }
        Token token = new Token();
        token.setToken(jwtUtil.generateToken(userPrincipal));
        token.setTokenExpDate(jwtUtil.generateExpirationDate());
        token.setCreatedBy(userPrincipal.getUserId());
        tokenService.createToken(token);
        List<Object> lstRes = new ArrayList<Object>();
        lstRes.add(token.getToken());
        lstRes.add(userPrincipal.getAuthorities());
        MLoginResponse newLoginRes = new MLoginResponse(token.getToken(), userPrincipal.getAuthorities(),
                userPrincipal.getUserId());
        return ResponseEntity.ok(newLoginRes);
    }

    // API quên mật khẩu
    @PutMapping("/resetPassword")
    public ResponseEntity<Object> resetPassword(@RequestBody User user) {
        try {
            User curentUser = userRepository.findByUsername(user.getUsername());
            if (curentUser == null) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Tài khoản không tồn tại, vui lòng kiểm tra lại.");
            }

            if (!curentUser.getEmail().equals(user.getEmail())) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Email không chính xác, vui lòng kiểm tra lại.");
            }

            if (!curentUser.getSecretAnswer().equals(user.getSecretAnswer())) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body("Câu trả lời bí mật không chính xác, vui lòng kiểm tra lại.");
            }

            int randomInt = (int) (Math.random() * (999999 - 111111 + 1)) + 111111;
            String newPassword = String.valueOf(randomInt);
            curentUser.setPassword(new BCryptPasswordEncoder().encode(newPassword));
            userRepository.saveAndFlush(curentUser);

            return ResponseEntity.status(HttpStatus.OK).body(newPassword);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Có lỗi xảy ra, vui lòng thử lại.");
        }

    }

    // hàm lấy thông tin user từ ID (Chỉ admin)
    @GetMapping("users/{id}")
    public ResponseEntity<Object> getUserByID(@PathVariable(value = "id") long id) {
        try {
            Optional<User> newUserOpt = userRepository.findById(id);
            if (newUserOpt.isPresent()) {
                return ResponseEntity.status(HttpStatus.OK).body(newUserOpt.get());
            } else
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Có lỗi xảy ra, vui lòng thử lại.");
        }
    }

    // API lấy dánh ách user
    @GetMapping("/usersLst")
    @CrossOrigin
    public ResponseEntity<Object> getAllUsser() {
        try {
            List<User> lstUser = userService.getAllUserService();
            return ResponseEntity.status(HttpStatus.OK).body(lstUser);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Có lỗi xảy ra, vui lòng thử lại.");
        }
    }

    // API khóa tài khoản
    @PutMapping("/users/lock/{id}")
    public ResponseEntity<Object> lockUser(@PathVariable long id) {
        try {
            User newUser = userService.lockUser(id);
            return ResponseEntity.status(HttpStatus.OK).body(null);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Có lỗi xảy ra, vui lòng thử lại.");
        }
    }

    @PutMapping("/users/update/{id}/{roleKey}")
    public ResponseEntity<Object> updateUser(@PathVariable(value = "id") long id, @RequestBody User user,
            @PathVariable(value = "roleKey") String roleKey) {
        try {
            User newUser = userService.updateUser(user, id, roleKey);
            return ResponseEntity.status(HttpStatus.OK).body(newUser);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Có lỗi xảy ra, vui lòng thử lại.");
        }
    }

    @PutMapping("/users/changePassword/{newPassword}")
    public ResponseEntity<Object> changePassword(@RequestBody User user, @PathVariable(value = "newPassword")String newPassword) {
        try {
            UserPrincipal userPrincipal = userService.findByUsername(user.getUsername());
            if (null == user || !new BCryptPasswordEncoder().matches(user.getPassword(), userPrincipal.getPassword())) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("tài khoản hoặc mật khẩu không chính xác");
            } else {
                userPrincipal.setPassword(new BCryptPasswordEncoder().encode(newPassword));
                User cUser = userRepository.findByUsername(user.getUsername());
                cUser.setPassword(new BCryptPasswordEncoder().encode(newPassword));
                userRepository.save(cUser);
                return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Đổi mật khẩu thành công");
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Có lỗi xảy ra, vui lòng thử lại.");
        }
    }
}
