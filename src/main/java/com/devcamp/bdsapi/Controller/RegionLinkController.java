package com.devcamp.bdsapi.Controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.*;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.devcamp.bdsapi.Entity.CRegionLink;
import com.devcamp.bdsapi.Respository.RegionLinkRepository;
import com.devcamp.bdsapi.Service.RegionLinkService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@CrossOrigin
public class RegionLinkController {
    @Autowired
    RegionLinkRepository regionLinkRepository;

    @Autowired
    RegionLinkService regionLinkService;

    @GetMapping("/regionLinks")
    public ResponseEntity<Object> getAllRegionLink() {
        try {
            List<CRegionLink> lstDes = regionLinkService.getAllRegionLinkService();
            return ResponseEntity.status(HttpStatus.OK).body(lstDes);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    @GetMapping("/regionLinks/{id}")
    public ResponseEntity<Object> getRegionLinkById(@PathVariable(value = "id") int id) {
        try {
            Optional<CRegionLink> desOptional = regionLinkService.getRegionLinkByIdService(id);
            if (desOptional.isPresent()) {
                return ResponseEntity.status(HttpStatus.OK).body(desOptional.get());
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    @PostMapping("/regionLinks")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> CreateRegionLink(@Valid @RequestBody CRegionLink cRegionLink) {

        CRegionLink newRegionLink = new CRegionLink();
        newRegionLink.setName(cRegionLink.getName());
        newRegionLink.setDescription(cRegionLink.getDescription());
        newRegionLink.set_lat(cRegionLink.get_lat());
        newRegionLink.set_lng(cRegionLink.get_lng());
        newRegionLink.setPhoto(cRegionLink.getPhoto());
        newRegionLink = regionLinkRepository.save(newRegionLink);
        return ResponseEntity.status(HttpStatus.CREATED).body(newRegionLink);
    }

    @PostMapping("/regionLinks2")
    public ResponseEntity<Object> CreateRegionLink(@RequestParam(value = "name") String name,
            @RequestParam(value = "address") int address,
            @RequestParam(value = "_lat") Double lat,
            @RequestParam(value = "_lng") Double lng,
            @RequestParam("image") MultipartFile multipartFile) throws IOException {
        //
        try {
            CRegionLink newRegionLink = new CRegionLink();
            newRegionLink.setName(name);
            newRegionLink.setAddress(address);
            newRegionLink.set_lat(lat);
            newRegionLink.set_lng(lng);
            newRegionLink.setName(name);

            String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
            newRegionLink.setPhoto(fileName);
            newRegionLink = regionLinkRepository.save(newRegionLink);

            String uploadDir = "uploads/";
            FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);

            return ResponseEntity.status(HttpStatus.CREATED).body(newRegionLink);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }

    }

    @PostMapping("/regionLinks3")
    public ResponseEntity<Object> CreateRegionLink3(@RequestParam(value = "name") String name,
            @RequestParam(value = "address") int address,
            @RequestParam(value = "_lat") Double lat,
            @RequestParam(value = "_lng") Double lng,
            @RequestParam("image") MultipartFile[] multipartFile) throws IOException {
        //
        try {
            CRegionLink newRegionLink = new CRegionLink();
            newRegionLink.setName(name);
            newRegionLink.setAddress(address);
            newRegionLink.set_lat(lat);
            newRegionLink.set_lng(lng);
            newRegionLink.setName(name);

            String regionPhoto = "";

            for (MultipartFile file : multipartFile) {
                String fileName = StringUtils.cleanPath(file.getOriginalFilename());
                newRegionLink = regionLinkRepository.save(newRegionLink);
                String uploadDir = "uploads/";
                FileUploadUtil.saveFile(uploadDir, fileName, file);
                regionPhoto = regionPhoto + ";" + fileName;
            }

            newRegionLink.setPhoto(regionPhoto);
            newRegionLink = regionLinkRepository.save(newRegionLink);

            return ResponseEntity.status(HttpStatus.CREATED).body(newRegionLink);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/regionLinks/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> updateRegionLink(@PathVariable(name = "id") int id,
            @Valid @RequestBody CRegionLink cRegionLink) {
        try {
            CRegionLink nCRegionLink = regionLinkService.updateRegionLinkService(id, cRegionLink);
            if (nCRegionLink != null) {
                return ResponseEntity.status(HttpStatus.OK).body(nCRegionLink);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    @DeleteMapping("/regionLinks/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> deleteDesignUnit(@PathVariable(name = "id") int id) {
        try {
            regionLinkService.deleteRegionLinkService(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create Staff: " + e.getCause().getCause().getMessage());
        }
    }
}
