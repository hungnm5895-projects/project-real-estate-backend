package com.devcamp.bdsapi.Controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bdsapi.Entity.CMasterLayout;
import com.devcamp.bdsapi.Service.MasterLayoutService;

@RestController
@CrossOrigin
public class MasterLayoutController {
    @Autowired
    MasterLayoutService masterLayoutService;

    @GetMapping("/masterLayouts")
    public ResponseEntity<Object> getAllMasterLayout() {
        try {
            List<CMasterLayout> lstMaster = masterLayoutService.getAllMasterLayoutService();
            return ResponseEntity.status(HttpStatus.OK).body(lstMaster);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    //Api truyền ID trả về chi tiết tiện ích
    @GetMapping("/masterLayouts/{id}")
    public ResponseEntity<Object> getMasterLayoutById(@PathVariable(value = "id") int id) {
        try {
            Optional<CMasterLayout> masterOptional = masterLayoutService.getMasterLayoutByIdService(id);
            if (masterOptional.isPresent()) {
                return ResponseEntity.status(HttpStatus.OK).body(masterOptional.get());
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    //API tạo mới tiện ích
    @PostMapping("/masterLayouts")
    public ResponseEntity<Object> createMasterLayout(@Valid @RequestBody CMasterLayout cMasterLayout) {
        try {
            CMasterLayout newMasterLayout = masterLayoutService.createMasterLayoutService(cMasterLayout);
            return ResponseEntity.status(HttpStatus.CREATED).body(newMasterLayout);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    //API cập nhật thông tin tiện ích
    @PutMapping("/masterLayouts/{id}")
    public ResponseEntity<Object> updateDesignUnit(@PathVariable(name = "id") int id,@Valid @RequestBody CMasterLayout cMasterLayout) {
        try {
            CMasterLayout newMas =  masterLayoutService.updateMasterLayoutService(id,cMasterLayout);
            if (newMas != null) {
                return ResponseEntity.status(HttpStatus.OK).body(newMas);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    //API xóa tiện ích
    @DeleteMapping("/masterLayouts/{id}")
    public ResponseEntity<Object> deleteMasterLayout(@PathVariable(name = "id") int id) {
        try {
            masterLayoutService.deleteMasterLayoutService(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed : " + e.getCause().getCause().getMessage());
        }
    }
}
