package com.devcamp.bdsapi.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bdsapi.Entity.CProject;
import com.devcamp.bdsapi.Service.ProjectService;

@RestController
@CrossOrigin
public class ProjectController {
    @Autowired
    ProjectService projectService;

    @GetMapping("/projects")
    public ResponseEntity<Object> getAllMasterLayout() {
        try {
            List<CProject> lstProject = projectService.getAllProjectService();
            return ResponseEntity.status(HttpStatus.OK).body(lstProject);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    // Api truyền ID trả về chi tiết projects
    @GetMapping("/projects/{id}")
    public ResponseEntity<Object> getProjectById(@PathVariable(value = "id") int id) {
        try {
            Optional<CProject> projectOptional = projectService.getProjectByIdService(id);
            if (projectOptional.isPresent()) {
                return ResponseEntity.status(HttpStatus.OK).body(projectOptional.get());
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    // API tạo mới projects
    @PostMapping("/projects")
    public ResponseEntity<Object> createProject(@RequestBody CProject cProject) {
        try {
            CProject newProject = projectService.createProjectService(cProject);
            return ResponseEntity.status(HttpStatus.CREATED).body(newProject);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    // API cập nhật thông tin projects
    @PutMapping("/projects/{id}")
    public ResponseEntity<Object> updateProject(@PathVariable(name = "id") int id,
            @RequestBody CProject cProject) {
        try {
            CProject savProject = projectService.updateProjectService(id, cProject);
            if (savProject != null) {
                return ResponseEntity.status(HttpStatus.OK).body(savProject);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    // API xóa projects
    @DeleteMapping("/projects/{id}")
    public ResponseEntity<Object> deleteProject(@PathVariable(name = "id") int id) {
        try {
            projectService.deleteProjectService(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed : " + e.getCause().getCause().getMessage());
        }
    }

    // API lấy danh sách project theo id của quận/huyện
    @GetMapping("/districts/{district_id}/projects")
    public ResponseEntity<Object> getProjectByDistrictId(@PathVariable(name = "district_id") int district_id) {
        try {
            List<CProject> lstProjects = projectService.getProjectListByDistrictId(district_id);
            return ResponseEntity.status(HttpStatus.OK).body(lstProjects);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Có lỗi xảy ra, vui lòng thử lại sau ");
        }
    }
}
