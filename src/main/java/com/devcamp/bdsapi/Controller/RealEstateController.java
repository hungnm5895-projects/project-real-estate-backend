package com.devcamp.bdsapi.Controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bdsapi.Entity.CRealEstate;
import com.devcamp.bdsapi.Model.MRealEstateResponse;
import com.devcamp.bdsapi.Service.RealEstateService;

@RestController
@CrossOrigin
public class RealEstateController {
    @Autowired
    RealEstateService realEstateService;

    @GetMapping("/realEstates")
    public ResponseEntity<Object> getAllRealEstates() {
        try {
            List<CRealEstate> lstRealEstates = realEstateService.getAllRealEstateService();
            return ResponseEntity.status(HttpStatus.OK).body(lstRealEstates);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    // Lấy danh sách BĐS phân trang
    @GetMapping("/realEstates2")
    public ResponseEntity<Object> getAllRealEstatesPaging(

            @RequestParam(value = "page", defaultValue = "0") String page,

            @RequestParam(value = "size", defaultValue = "6") String size) {
        try {
            List<CRealEstate> lstRealEstates = realEstateService.getAllRealEstatePagingService(page, size);
            return ResponseEntity.status(HttpStatus.OK).body(lstRealEstates);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " +
                    e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    // Lấy danh sách BĐS hot
    @GetMapping("/realEstatesHot")
    public ResponseEntity<Object> getHotRealEstates() {
        try {
            List<CRealEstate> lstHot = realEstateService.getHotRealEstatePagingService();
            return ResponseEntity.status(HttpStatus.OK).body(lstHot);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " +
                    e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    // Api truyền ID trả về chi tiết realEstates
    @GetMapping("/realEstates/{id}")
    public ResponseEntity<Object> getRealEstateById(@PathVariable(value = "id") int id) {
        try {
            Optional<CRealEstate> realOptional = realEstateService.getRealEstateByIdService(id);
            if (realOptional.isPresent()) {
                return ResponseEntity.status(HttpStatus.OK).body(realOptional.get());
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    // API tạo mới RealEstate
    @PostMapping("/realEstates")
    public ResponseEntity<Object> createRealEstate(@Valid @RequestBody CRealEstate cRealEstate) {
        try {
            CRealEstate newRealEstate = realEstateService.createRealEstateService(cRealEstate);
            return ResponseEntity.status(HttpStatus.CREATED).body(newRealEstate);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    // API cập nhật thông tin RealEstate cho người dùng thông thường
    @PutMapping("/realEstates/{id}")
    public ResponseEntity<Object> updateRealEstate(@Valid @PathVariable(name = "id") int id,
            @RequestBody CRealEstate cRealEstate) {
        try {
            CRealEstate saveRealEstate = realEstateService.updateRealEstateService(id, cRealEstate);
            if (saveRealEstate != null) {
                return ResponseEntity.status(HttpStatus.OK).body(saveRealEstate);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/realEstates/admin/{id}")
    public ResponseEntity<Object> updateRealEstateAdmin(@Valid @PathVariable(name = "id") int id,
            @RequestBody CRealEstate cRealEstate) {
        try {
            CRealEstate saveRealEstate = realEstateService.updateRealEstateAdminService(id, cRealEstate);
            if (saveRealEstate != null) {
                return ResponseEntity.status(HttpStatus.OK).body(saveRealEstate);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed: " + e.getCause().getCause().getMessage());
        }
    }

    // API xóa RealEstate
    @DeleteMapping("/realEstates/{id}")
    public ResponseEntity<Object> deleteRealEstate(@PathVariable(name = "id") int id) {
        try {
            realEstateService.deleteRealEstateService(id);
            ;
            ;
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed : " + e.getCause().getCause().getMessage());
        }
    }

    // API cho phép lọc dữ liệu
    @GetMapping("/realEstatesFilters")
    public ResponseEntity<Object> getFilterRealEstatesPaging(

            @RequestParam(value = "page", defaultValue = "0") String page,
            @RequestParam(value = "size", defaultValue = "6") String size,
            @RequestParam(value = "type", defaultValue = "-1") int type,
            @RequestParam(value = "request", defaultValue = "-1") int request,
            @RequestParam(value = "province_id", defaultValue = "0") int province_id,
            @RequestParam(value = "district_id", defaultValue = "0") int district_id,
            @RequestParam(value = "furniture_type", defaultValue = "-1") int furniture_type) {
        try {
            List<CRealEstate> lstRealEstates = realEstateService.getFilterRealEstatePagingService(page, size, type,
                    request, province_id, district_id, furniture_type);
            List<CRealEstate> lstAll = realEstateService.getFilterRealEstatePagingService("0", "999999", type,
                    request, province_id, district_id, furniture_type);
            MRealEstateResponse res = new MRealEstateResponse(lstRealEstates, lstAll.size());

            return ResponseEntity.status(HttpStatus.OK).body(res);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " +
                    e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Đã có lỗi xảy ra, vui lòng thử lại ");
        }
    }

    // Truy vấn BDS theo ID người đăng và theo status
    @GetMapping("/realEstates/AllStatus/{id}/{status}")
    public ResponseEntity<Object> getRealEstatesStatusPaging(

            @PathVariable(value = "id") int create_by,
            @PathVariable(value = "status") int status,
            @RequestParam(value = "page", defaultValue = "0") String page,
            @RequestParam(value = "size", defaultValue = "8") String size) {
        try {
            List<CRealEstate> lstRealEstates = realEstateService.getRealEstateByIdAndStatusService(page, size,
                    create_by, status);

            List<CRealEstate> all = realEstateService.getRealEstateByIdAndStatusService("0", "99999",
                    create_by, status);

            MRealEstateResponse newRes = new MRealEstateResponse(lstRealEstates, all.size());
            return ResponseEntity.status(HttpStatus.OK).body(newRes);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " +
                    e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Đã có lỗi xảy ra, vui lòng thử lại ");
        }
    }

    // Truy vấn toàn bộ BDS theo đã được duyệt
    @GetMapping("/realEstates/index")
    public ResponseEntity<Object> getAllRealEstatesIndex(

            @RequestParam(value = "page", defaultValue = "0") String page,
            @RequestParam(value = "size", defaultValue = "8") String size) {
        try {
            List<CRealEstate> lstRealEstates = realEstateService.getAllRealEstatePagingIndexService(page, size);
            List<CRealEstate> all = realEstateService.getAllRealEstatePagingIndexService("0", "99999");
            MRealEstateResponse newRes = new MRealEstateResponse(lstRealEstates, all.size());
            return ResponseEntity.status(HttpStatus.OK).body(newRes);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " +
                    e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Đã có lỗi xảy ra, vui lòng thử lại ");
        }
    }

    // Truy vấn toàn bộ BDS theo ID đăng bằng
    @GetMapping("/realEstates/creatBY/{id}")
    public ResponseEntity<Object> getAllRealEstatesByCreateById(            
            @PathVariable(value = "id") int id) {
        try {
            List<CRealEstate> lstRealEstates = realEstateService.getRealEstateByCreateId(id);
            return ResponseEntity.status(HttpStatus.OK).body(lstRealEstates);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " +
                    e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Đã có lỗi xảy ra, vui lòng thử lại ");
        }
    }

    // Truy vấn toàn bộ BDS theo ID của khách hàng
    @GetMapping("/realEstates/customer/{id}")
    public ResponseEntity<Object> getAllRealEstatesByCustomerId(            
            @PathVariable(value = "id") int id) {
        try {
            List<CRealEstate> lstRealEstates = realEstateService.getRealEstateByCustomerId(id);
            return ResponseEntity.status(HttpStatus.OK).body(lstRealEstates);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " +
                    e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Đã có lỗi xảy ra, vui lòng thử lại ");
        }
    }

    // Truy vấn toàn bộ BDS theo ID của khách hàng
    @GetMapping("/realEstates/customer2/{id}")
    public ResponseEntity<Object> getAllRealEstatesByCustomerId2(            
            @PathVariable(value = "id") int id) {
        try {
            List<CRealEstate> lstRealEstates = realEstateService.getRealEstateByCustomerId(id);
            return ResponseEntity.status(HttpStatus.OK).body(lstRealEstates.size());
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " +
                    e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Đã có lỗi xảy ra, vui lòng thử lại ");
        }
    }
}
